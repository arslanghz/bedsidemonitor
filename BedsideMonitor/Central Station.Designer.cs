﻿using System;
using System.Windows.Forms;

namespace BedsideMonitor
{
    partial class frmCentralStation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCentralStation));
            this.p1 = new System.Windows.Forms.Panel();
            this.bp1s = new System.Windows.Forms.Label();
            this.bp1d = new System.Windows.Forms.Label();
            this.te1s = new System.Windows.Forms.Label();
            this.te1d = new System.Windows.Forms.Label();
            this.pr1s = new System.Windows.Forms.Label();
            this.pr1d = new System.Windows.Forms.Label();
            this.br1s = new System.Windows.Forms.Label();
            this.br1d = new System.Windows.Forms.Label();
            this.l1re = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.l1Bpd = new System.Windows.Forms.Label();
            this.l1Bps = new System.Windows.Forms.Label();
            this.l1Te = new System.Windows.Forms.Label();
            this.l1Pr = new System.Windows.Forms.Label();
            this.l1Br = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.m1 = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bpd1Max = new System.Windows.Forms.NumericUpDown();
            this.bpd1Min = new System.Windows.Forms.NumericUpDown();
            this.bps1Max = new System.Windows.Forms.NumericUpDown();
            this.bps1Min = new System.Windows.Forms.NumericUpDown();
            this.te1max = new System.Windows.Forms.NumericUpDown();
            this.te1Min = new System.Windows.Forms.NumericUpDown();
            this.pr1Max = new System.Windows.Forms.NumericUpDown();
            this.pr1Min = new System.Windows.Forms.NumericUpDown();
            this.br1Max = new System.Windows.Forms.NumericUpDown();
            this.br1Min = new System.Windows.Forms.NumericUpDown();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.l2Bpd = new System.Windows.Forms.Label();
            this.l2Bps = new System.Windows.Forms.Label();
            this.l2Te = new System.Windows.Forms.Label();
            this.l2Pr = new System.Windows.Forms.Label();
            this.l2Br = new System.Windows.Forms.Label();
            this.m2 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.bpd2Max = new System.Windows.Forms.NumericUpDown();
            this.bpd2Min = new System.Windows.Forms.NumericUpDown();
            this.bps2Max = new System.Windows.Forms.NumericUpDown();
            this.bps2Min = new System.Windows.Forms.NumericUpDown();
            this.te2Max = new System.Windows.Forms.NumericUpDown();
            this.te2Min = new System.Windows.Forms.NumericUpDown();
            this.pr2Max = new System.Windows.Forms.NumericUpDown();
            this.pr2Min = new System.Windows.Forms.NumericUpDown();
            this.br2Max = new System.Windows.Forms.NumericUpDown();
            this.br2Min = new System.Windows.Forms.NumericUpDown();
            this.p2 = new System.Windows.Forms.Panel();
            this.l2re = new System.Windows.Forms.Button();
            this.bp2s = new System.Windows.Forms.Label();
            this.bp2d = new System.Windows.Forms.Label();
            this.te2s = new System.Windows.Forms.Label();
            this.te2d = new System.Windows.Forms.Label();
            this.pr2s = new System.Windows.Forms.Label();
            this.pr2d = new System.Windows.Forms.Label();
            this.br2s = new System.Windows.Forms.Label();
            this.br2d = new System.Windows.Forms.Label();
            this.p3 = new System.Windows.Forms.Panel();
            this.l3re = new System.Windows.Forms.Button();
            this.bp3s = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bp3d = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.te3s = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.te3d = new System.Windows.Forms.Label();
            this.l3Bpd = new System.Windows.Forms.Label();
            this.pr3s = new System.Windows.Forms.Label();
            this.pr3d = new System.Windows.Forms.Label();
            this.l3Bps = new System.Windows.Forms.Label();
            this.br3s = new System.Windows.Forms.Label();
            this.l3Te = new System.Windows.Forms.Label();
            this.br3d = new System.Windows.Forms.Label();
            this.l3Pr = new System.Windows.Forms.Label();
            this.l3Br = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.m3 = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.bpd3Max = new System.Windows.Forms.NumericUpDown();
            this.bpd3Min = new System.Windows.Forms.NumericUpDown();
            this.bps3Max = new System.Windows.Forms.NumericUpDown();
            this.bps3Min = new System.Windows.Forms.NumericUpDown();
            this.te3Max = new System.Windows.Forms.NumericUpDown();
            this.te3Min = new System.Windows.Forms.NumericUpDown();
            this.pr3Max = new System.Windows.Forms.NumericUpDown();
            this.pr3Min = new System.Windows.Forms.NumericUpDown();
            this.br3Max = new System.Windows.Forms.NumericUpDown();
            this.br3Min = new System.Windows.Forms.NumericUpDown();
            this.p4 = new System.Windows.Forms.Panel();
            this.l4re = new System.Windows.Forms.Button();
            this.bp4s = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.bp4d = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.te4s = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.te4d = new System.Windows.Forms.Label();
            this.l4Bpd = new System.Windows.Forms.Label();
            this.pr4s = new System.Windows.Forms.Label();
            this.pr4d = new System.Windows.Forms.Label();
            this.l4Bps = new System.Windows.Forms.Label();
            this.br4s = new System.Windows.Forms.Label();
            this.l4Te = new System.Windows.Forms.Label();
            this.br4d = new System.Windows.Forms.Label();
            this.l4Pr = new System.Windows.Forms.Label();
            this.l4Br = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.m4 = new System.Windows.Forms.CheckBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.bpd4Max = new System.Windows.Forms.NumericUpDown();
            this.bpd4Min = new System.Windows.Forms.NumericUpDown();
            this.bps4Max = new System.Windows.Forms.NumericUpDown();
            this.bps4Min = new System.Windows.Forms.NumericUpDown();
            this.te4Max = new System.Windows.Forms.NumericUpDown();
            this.te4Min = new System.Windows.Forms.NumericUpDown();
            this.pr4Max = new System.Windows.Forms.NumericUpDown();
            this.pr4Min = new System.Windows.Forms.NumericUpDown();
            this.br4Max = new System.Windows.Forms.NumericUpDown();
            this.br4Min = new System.Windows.Forms.NumericUpDown();
            this.p5 = new System.Windows.Forms.Panel();
            this.l5re = new System.Windows.Forms.Button();
            this.bp5s = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.bp5d = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.te5s = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.te5d = new System.Windows.Forms.Label();
            this.l5Bpd = new System.Windows.Forms.Label();
            this.pr5s = new System.Windows.Forms.Label();
            this.pr5d = new System.Windows.Forms.Label();
            this.l5Bps = new System.Windows.Forms.Label();
            this.br5s = new System.Windows.Forms.Label();
            this.l5Te = new System.Windows.Forms.Label();
            this.br5d = new System.Windows.Forms.Label();
            this.l5Pr = new System.Windows.Forms.Label();
            this.l5Br = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.m5 = new System.Windows.Forms.CheckBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.bpd5Max = new System.Windows.Forms.NumericUpDown();
            this.bpd5Min = new System.Windows.Forms.NumericUpDown();
            this.bps5Max = new System.Windows.Forms.NumericUpDown();
            this.bps5Min = new System.Windows.Forms.NumericUpDown();
            this.te5Max = new System.Windows.Forms.NumericUpDown();
            this.te5Min = new System.Windows.Forms.NumericUpDown();
            this.pr5Max = new System.Windows.Forms.NumericUpDown();
            this.pr5Min = new System.Windows.Forms.NumericUpDown();
            this.br5Max = new System.Windows.Forms.NumericUpDown();
            this.br5Min = new System.Windows.Forms.NumericUpDown();
            this.p6 = new System.Windows.Forms.Panel();
            this.l6re = new System.Windows.Forms.Button();
            this.bp6s = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.bp6d = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.te6s = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.te6d = new System.Windows.Forms.Label();
            this.l6Bpd = new System.Windows.Forms.Label();
            this.pr6s = new System.Windows.Forms.Label();
            this.l6Bps = new System.Windows.Forms.Label();
            this.pr6d = new System.Windows.Forms.Label();
            this.l6Te = new System.Windows.Forms.Label();
            this.br6s = new System.Windows.Forms.Label();
            this.br6d = new System.Windows.Forms.Label();
            this.l6Pr = new System.Windows.Forms.Label();
            this.l6Br = new System.Windows.Forms.Label();
            this.m6 = new System.Windows.Forms.Label();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.bpd6Max = new System.Windows.Forms.NumericUpDown();
            this.bpd6Min = new System.Windows.Forms.NumericUpDown();
            this.bps6Max = new System.Windows.Forms.NumericUpDown();
            this.bps6Min = new System.Windows.Forms.NumericUpDown();
            this.te6Max = new System.Windows.Forms.NumericUpDown();
            this.te6Min = new System.Windows.Forms.NumericUpDown();
            this.pr6Max = new System.Windows.Forms.NumericUpDown();
            this.pr6Min = new System.Windows.Forms.NumericUpDown();
            this.br6Max = new System.Windows.Forms.NumericUpDown();
            this.br6Min = new System.Windows.Forms.NumericUpDown();
            this.p7 = new System.Windows.Forms.Panel();
            this.l7re = new System.Windows.Forms.Button();
            this.bp7s = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.bp7d = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.te7s = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.te7d = new System.Windows.Forms.Label();
            this.l7Bpd = new System.Windows.Forms.Label();
            this.pr7s = new System.Windows.Forms.Label();
            this.l7Bps = new System.Windows.Forms.Label();
            this.pr7d = new System.Windows.Forms.Label();
            this.l7Te = new System.Windows.Forms.Label();
            this.br7s = new System.Windows.Forms.Label();
            this.br7d = new System.Windows.Forms.Label();
            this.l7Pr = new System.Windows.Forms.Label();
            this.l7Br = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.m7 = new System.Windows.Forms.CheckBox();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.bpd7Max = new System.Windows.Forms.NumericUpDown();
            this.bpd7Min = new System.Windows.Forms.NumericUpDown();
            this.bps7Max = new System.Windows.Forms.NumericUpDown();
            this.bps7Min = new System.Windows.Forms.NumericUpDown();
            this.te7Max = new System.Windows.Forms.NumericUpDown();
            this.te7Min = new System.Windows.Forms.NumericUpDown();
            this.pr7Max = new System.Windows.Forms.NumericUpDown();
            this.pr7Min = new System.Windows.Forms.NumericUpDown();
            this.br7Max = new System.Windows.Forms.NumericUpDown();
            this.br7Min = new System.Windows.Forms.NumericUpDown();
            this.p8 = new System.Windows.Forms.Panel();
            this.l8re = new System.Windows.Forms.Button();
            this.bp8s = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.bp8d = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.te8s = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.te8d = new System.Windows.Forms.Label();
            this.l8Bpd = new System.Windows.Forms.Label();
            this.pr8s = new System.Windows.Forms.Label();
            this.l8Bps = new System.Windows.Forms.Label();
            this.pr8d = new System.Windows.Forms.Label();
            this.l8Te = new System.Windows.Forms.Label();
            this.br8s = new System.Windows.Forms.Label();
            this.br8d = new System.Windows.Forms.Label();
            this.l8Pr = new System.Windows.Forms.Label();
            this.l8Br = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.m8 = new System.Windows.Forms.CheckBox();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.bpd8Max = new System.Windows.Forms.NumericUpDown();
            this.bpd8Min = new System.Windows.Forms.NumericUpDown();
            this.bps8Max = new System.Windows.Forms.NumericUpDown();
            this.bps8Min = new System.Windows.Forms.NumericUpDown();
            this.te8Max = new System.Windows.Forms.NumericUpDown();
            this.te8Min = new System.Windows.Forms.NumericUpDown();
            this.pr8Max = new System.Windows.Forms.NumericUpDown();
            this.pr8Min = new System.Windows.Forms.NumericUpDown();
            this.br8Max = new System.Windows.Forms.NumericUpDown();
            this.br8Min = new System.Windows.Forms.NumericUpDown();
            this.a1 = new System.Windows.Forms.Button();
            this.a3 = new System.Windows.Forms.Button();
            this.a4 = new System.Windows.Forms.Button();
            this.a5 = new System.Windows.Forms.Button();
            this.a6 = new System.Windows.Forms.Button();
            this.a7 = new System.Windows.Forms.Button();
            this.a8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.shiftBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bedsideMonitorDataSet1 = new BedsideMonitor.BedsideMonitorDataSet1();
            this.shiftTableAdapter = new BedsideMonitor.BedsideMonitorDataSet1TableAdapters.ShiftTableAdapter();
            this.a2 = new System.Windows.Forms.Button();
            this.p1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te1max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br2Min)).BeginInit();
            this.p2.SuspendLayout();
            this.p3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br3Min)).BeginInit();
            this.p4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br4Min)).BeginInit();
            this.p5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd5Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd5Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps5Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps5Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te5Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te5Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr5Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr5Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br5Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br5Min)).BeginInit();
            this.p6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd6Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd6Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps6Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps6Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te6Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te6Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr6Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr6Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br6Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br6Min)).BeginInit();
            this.p7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd7Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd7Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps7Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps7Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te7Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te7Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr7Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr7Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br7Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br7Min)).BeginInit();
            this.p8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd8Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd8Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps8Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps8Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te8Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.te8Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr8Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr8Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br8Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.br8Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shiftBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bedsideMonitorDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // p1
            // 
            this.p1.AccessibleDescription = "";
            this.p1.AccessibleName = "";
            this.p1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("p1.BackgroundImage")));
            this.p1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.p1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.p1.Controls.Add(this.bp1s);
            this.p1.Controls.Add(this.bp1d);
            this.p1.Controls.Add(this.te1s);
            this.p1.Controls.Add(this.te1d);
            this.p1.Controls.Add(this.pr1s);
            this.p1.Controls.Add(this.pr1d);
            this.p1.Controls.Add(this.br1s);
            this.p1.Controls.Add(this.br1d);
            this.p1.Controls.Add(this.l1re);
            this.p1.Controls.Add(this.label16);
            this.p1.Controls.Add(this.label15);
            this.p1.Controls.Add(this.label13);
            this.p1.Controls.Add(this.l1Bpd);
            this.p1.Controls.Add(this.l1Bps);
            this.p1.Controls.Add(this.l1Te);
            this.p1.Controls.Add(this.l1Pr);
            this.p1.Controls.Add(this.l1Br);
            this.p1.Controls.Add(this.label14);
            this.p1.Controls.Add(this.m1);
            this.p1.Controls.Add(this.label12);
            this.p1.Controls.Add(this.label11);
            this.p1.Controls.Add(this.label10);
            this.p1.Controls.Add(this.label9);
            this.p1.Controls.Add(this.label8);
            this.p1.Controls.Add(this.label7);
            this.p1.Controls.Add(this.label6);
            this.p1.Controls.Add(this.bpd1Max);
            this.p1.Controls.Add(this.bpd1Min);
            this.p1.Controls.Add(this.bps1Max);
            this.p1.Controls.Add(this.bps1Min);
            this.p1.Controls.Add(this.te1max);
            this.p1.Controls.Add(this.te1Min);
            this.p1.Controls.Add(this.pr1Max);
            this.p1.Controls.Add(this.pr1Min);
            this.p1.Controls.Add(this.br1Max);
            this.p1.Controls.Add(this.br1Min);
            this.p1.Location = new System.Drawing.Point(12, 33);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(259, 240);
            this.p1.TabIndex = 0;
            this.p1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // bp1s
            // 
            this.bp1s.AutoSize = true;
            this.bp1s.Location = new System.Drawing.Point(216, 177);
            this.bp1s.Name = "bp1s";
            this.bp1s.Size = new System.Drawing.Size(34, 13);
            this.bp1s.TabIndex = 60;
            this.bp1s.Text = "Selec";
            this.bp1s.Click += new System.EventHandler(this.bp1s_Click);
            // 
            // bp1d
            // 
            this.bp1d.AutoSize = true;
            this.bp1d.Location = new System.Drawing.Point(202, 177);
            this.bp1d.Name = "bp1d";
            this.bp1d.Size = new System.Drawing.Size(54, 13);
            this.bp1d.TabIndex = 59;
            this.bp1d.Text = "De Select";
            this.bp1d.Click += new System.EventHandler(this.bp1d_Click);
            // 
            // te1s
            // 
            this.te1s.AutoSize = true;
            this.te1s.Location = new System.Drawing.Point(216, 130);
            this.te1s.Name = "te1s";
            this.te1s.Size = new System.Drawing.Size(34, 13);
            this.te1s.TabIndex = 58;
            this.te1s.Text = "Selec";
            this.te1s.Click += new System.EventHandler(this.te1s_Click);
            // 
            // te1d
            // 
            this.te1d.AutoSize = true;
            this.te1d.Location = new System.Drawing.Point(202, 130);
            this.te1d.Name = "te1d";
            this.te1d.Size = new System.Drawing.Size(54, 13);
            this.te1d.TabIndex = 57;
            this.te1d.Text = "De Select";
            this.te1d.Click += new System.EventHandler(this.te1d_Click);
            // 
            // pr1s
            // 
            this.pr1s.AutoSize = true;
            this.pr1s.Location = new System.Drawing.Point(216, 88);
            this.pr1s.Name = "pr1s";
            this.pr1s.Size = new System.Drawing.Size(34, 13);
            this.pr1s.TabIndex = 56;
            this.pr1s.Text = "Selec";
            this.pr1s.Click += new System.EventHandler(this.pr1s_Click);
            // 
            // pr1d
            // 
            this.pr1d.AutoSize = true;
            this.pr1d.Location = new System.Drawing.Point(202, 88);
            this.pr1d.Name = "pr1d";
            this.pr1d.Size = new System.Drawing.Size(54, 13);
            this.pr1d.TabIndex = 55;
            this.pr1d.Text = "De Select";
            this.pr1d.Click += new System.EventHandler(this.pr1d_Click);
            // 
            // br1s
            // 
            this.br1s.AutoSize = true;
            this.br1s.Location = new System.Drawing.Point(216, 47);
            this.br1s.Name = "br1s";
            this.br1s.Size = new System.Drawing.Size(37, 13);
            this.br1s.TabIndex = 54;
            this.br1s.Text = "Select";
            this.br1s.Click += new System.EventHandler(this.br1U_Click);
            // 
            // br1d
            // 
            this.br1d.AutoSize = true;
            this.br1d.Location = new System.Drawing.Point(202, 47);
            this.br1d.Name = "br1d";
            this.br1d.Size = new System.Drawing.Size(54, 13);
            this.br1d.TabIndex = 50;
            this.br1d.Text = "De Select";
            this.br1d.Click += new System.EventHandler(this.bR1h_Click);
            // 
            // l1re
            // 
            this.l1re.Location = new System.Drawing.Point(205, 215);
            this.l1re.Name = "l1re";
            this.l1re.Size = new System.Drawing.Size(44, 22);
            this.l1re.TabIndex = 49;
            this.l1re.Text = "Reset";
            this.l1re.UseVisualStyleBackColor = true;
            this.l1re.Click += new System.EventHandler(this.l1Re_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 93);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 13);
            this.label16.TabIndex = 47;
            this.label16.Text = "Rate";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(20, 53);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 46;
            this.label15.Text = "Rate";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(173, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 45;
            this.label13.Text = "Current";
            // 
            // l1Bpd
            // 
            this.l1Bpd.AutoSize = true;
            this.l1Bpd.Location = new System.Drawing.Point(176, 217);
            this.l1Bpd.Name = "l1Bpd";
            this.l1Bpd.Size = new System.Drawing.Size(19, 13);
            this.l1Bpd.TabIndex = 44;
            this.l1Bpd.Text = "70";
            // 
            // l1Bps
            // 
            this.l1Bps.AutoSize = true;
            this.l1Bps.Location = new System.Drawing.Point(176, 175);
            this.l1Bps.Name = "l1Bps";
            this.l1Bps.Size = new System.Drawing.Size(25, 13);
            this.l1Bps.TabIndex = 43;
            this.l1Bps.Text = "105";
            // 
            // l1Te
            // 
            this.l1Te.AutoSize = true;
            this.l1Te.Location = new System.Drawing.Point(176, 130);
            this.l1Te.Name = "l1Te";
            this.l1Te.Size = new System.Drawing.Size(28, 13);
            this.l1Te.TabIndex = 42;
            this.l1Te.Text = "36.9";
            // 
            // l1Pr
            // 
            this.l1Pr.AutoSize = true;
            this.l1Pr.Location = new System.Drawing.Point(176, 87);
            this.l1Pr.Name = "l1Pr";
            this.l1Pr.Size = new System.Drawing.Size(19, 13);
            this.l1Pr.TabIndex = 41;
            this.l1Pr.Text = "80";
            this.l1Pr.Click += new System.EventHandler(this.lblPr_Click);
            // 
            // l1Br
            // 
            this.l1Br.AutoSize = true;
            this.l1Br.Location = new System.Drawing.Point(176, 46);
            this.l1Br.Name = "l1Br";
            this.l1Br.Size = new System.Drawing.Size(19, 13);
            this.l1Br.TabIndex = 40;
            this.l1Br.Text = "16";
            this.l1Br.Click += new System.EventHandler(this.shiftBindingSource_CurrentChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(212, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "Bed 1";
            // 
            // m1
            // 
            this.m1.AutoSize = true;
            this.m1.Location = new System.Drawing.Point(11, 8);
            this.m1.Name = "m1";
            this.m1.Size = new System.Drawing.Size(50, 17);
            this.m1.TabIndex = 38;
            this.m1.Text = "Mute";
            this.m1.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(71, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "Min";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(122, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 35;
            this.label11.Text = "Max";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 217);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "BP Diastolic";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 175);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "BP Systolic";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Temperature";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Pulse";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Breathing";
            // 
            // bpd1Max
            // 
            this.bpd1Max.Location = new System.Drawing.Point(125, 215);
            this.bpd1Max.Name = "bpd1Max";
            this.bpd1Max.Size = new System.Drawing.Size(45, 20);
            this.bpd1Max.TabIndex = 24;
            this.bpd1Max.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // bpd1Min
            // 
            this.bpd1Min.Location = new System.Drawing.Point(74, 215);
            this.bpd1Min.Name = "bpd1Min";
            this.bpd1Min.Size = new System.Drawing.Size(45, 20);
            this.bpd1Min.TabIndex = 23;
            this.bpd1Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // bps1Max
            // 
            this.bps1Max.Location = new System.Drawing.Point(125, 173);
            this.bps1Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.bps1Max.Name = "bps1Max";
            this.bps1Max.Size = new System.Drawing.Size(45, 20);
            this.bps1Max.TabIndex = 22;
            this.bps1Max.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // bps1Min
            // 
            this.bps1Min.Location = new System.Drawing.Point(74, 173);
            this.bps1Min.Name = "bps1Min";
            this.bps1Min.Size = new System.Drawing.Size(45, 20);
            this.bps1Min.TabIndex = 21;
            this.bps1Min.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // te1max
            // 
            this.te1max.DecimalPlaces = 1;
            this.te1max.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te1max.Location = new System.Drawing.Point(125, 128);
            this.te1max.Name = "te1max";
            this.te1max.Size = new System.Drawing.Size(45, 20);
            this.te1max.TabIndex = 20;
            this.te1max.Value = new decimal(new int[] {
            372,
            0,
            0,
            65536});
            // 
            // te1Min
            // 
            this.te1Min.DecimalPlaces = 1;
            this.te1Min.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te1Min.Location = new System.Drawing.Point(74, 128);
            this.te1Min.Name = "te1Min";
            this.te1Min.Size = new System.Drawing.Size(45, 20);
            this.te1Min.TabIndex = 19;
            this.te1Min.Value = new decimal(new int[] {
            365,
            0,
            0,
            65536});
            // 
            // pr1Max
            // 
            this.pr1Max.Location = new System.Drawing.Point(125, 85);
            this.pr1Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.pr1Max.Name = "pr1Max";
            this.pr1Max.Size = new System.Drawing.Size(45, 20);
            this.pr1Max.TabIndex = 18;
            this.pr1Max.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pr1Min
            // 
            this.pr1Min.Location = new System.Drawing.Point(74, 85);
            this.pr1Min.Name = "pr1Min";
            this.pr1Min.Size = new System.Drawing.Size(45, 20);
            this.pr1Min.TabIndex = 17;
            this.pr1Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // br1Max
            // 
            this.br1Max.Location = new System.Drawing.Point(125, 44);
            this.br1Max.Name = "br1Max";
            this.br1Max.Size = new System.Drawing.Size(45, 20);
            this.br1Max.TabIndex = 16;
            this.br1Max.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // br1Min
            // 
            this.br1Min.Location = new System.Drawing.Point(74, 44);
            this.br1Min.Name = "br1Min";
            this.br1Min.Size = new System.Drawing.Size(45, 20);
            this.br1Min.TabIndex = 15;
            this.br1Min.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 2000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "Rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "Rate";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(172, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 45;
            this.label3.Text = "Current";
            // 
            // l2Bpd
            // 
            this.l2Bpd.AutoSize = true;
            this.l2Bpd.Location = new System.Drawing.Point(175, 217);
            this.l2Bpd.Name = "l2Bpd";
            this.l2Bpd.Size = new System.Drawing.Size(19, 13);
            this.l2Bpd.TabIndex = 44;
            this.l2Bpd.Text = "70";
            // 
            // l2Bps
            // 
            this.l2Bps.AutoSize = true;
            this.l2Bps.Location = new System.Drawing.Point(175, 175);
            this.l2Bps.Name = "l2Bps";
            this.l2Bps.Size = new System.Drawing.Size(25, 13);
            this.l2Bps.TabIndex = 43;
            this.l2Bps.Text = "105";
            // 
            // l2Te
            // 
            this.l2Te.AutoSize = true;
            this.l2Te.Location = new System.Drawing.Point(175, 130);
            this.l2Te.Name = "l2Te";
            this.l2Te.Size = new System.Drawing.Size(28, 13);
            this.l2Te.TabIndex = 42;
            this.l2Te.Text = "36.9";
            // 
            // l2Pr
            // 
            this.l2Pr.AutoSize = true;
            this.l2Pr.Location = new System.Drawing.Point(175, 87);
            this.l2Pr.Name = "l2Pr";
            this.l2Pr.Size = new System.Drawing.Size(19, 13);
            this.l2Pr.TabIndex = 41;
            this.l2Pr.Text = "80";
            // 
            // l2Br
            // 
            this.l2Br.AutoSize = true;
            this.l2Br.Location = new System.Drawing.Point(175, 46);
            this.l2Br.Name = "l2Br";
            this.l2Br.Size = new System.Drawing.Size(19, 13);
            this.l2Br.TabIndex = 40;
            this.l2Br.Text = "16";
            // 
            // m2
            // 
            this.m2.AutoSize = true;
            this.m2.Location = new System.Drawing.Point(219, 4);
            this.m2.Name = "m2";
            this.m2.Size = new System.Drawing.Size(35, 13);
            this.m2.TabIndex = 39;
            this.m2.Text = "Bed 2";
            this.m2.Click += new System.EventHandler(this.label20_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(10, 8);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(50, 17);
            this.checkBox2.TabIndex = 38;
            this.checkBox2.Text = "Mute";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(70, 28);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(24, 13);
            this.label21.TabIndex = 36;
            this.label21.Text = "Min";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(121, 28);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 13);
            this.label22.TabIndex = 35;
            this.label22.Text = "Max";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(8, 217);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(64, 13);
            this.label23.TabIndex = 34;
            this.label23.Text = "BP Diastolic";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(8, 175);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "BP Systolic";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(4, 130);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 13);
            this.label25.TabIndex = 32;
            this.label25.Text = "Temperature";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(16, 80);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 13);
            this.label26.TabIndex = 31;
            this.label26.Text = "Pulse";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(8, 40);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(52, 13);
            this.label27.TabIndex = 30;
            this.label27.Text = "Breathing";
            // 
            // bpd2Max
            // 
            this.bpd2Max.Location = new System.Drawing.Point(124, 215);
            this.bpd2Max.Name = "bpd2Max";
            this.bpd2Max.Size = new System.Drawing.Size(45, 20);
            this.bpd2Max.TabIndex = 24;
            this.bpd2Max.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // bpd2Min
            // 
            this.bpd2Min.Location = new System.Drawing.Point(73, 215);
            this.bpd2Min.Name = "bpd2Min";
            this.bpd2Min.Size = new System.Drawing.Size(45, 20);
            this.bpd2Min.TabIndex = 23;
            this.bpd2Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // bps2Max
            // 
            this.bps2Max.Location = new System.Drawing.Point(124, 173);
            this.bps2Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.bps2Max.Name = "bps2Max";
            this.bps2Max.Size = new System.Drawing.Size(45, 20);
            this.bps2Max.TabIndex = 22;
            this.bps2Max.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // bps2Min
            // 
            this.bps2Min.Location = new System.Drawing.Point(73, 173);
            this.bps2Min.Name = "bps2Min";
            this.bps2Min.Size = new System.Drawing.Size(45, 20);
            this.bps2Min.TabIndex = 21;
            this.bps2Min.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // te2Max
            // 
            this.te2Max.DecimalPlaces = 1;
            this.te2Max.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te2Max.Location = new System.Drawing.Point(124, 128);
            this.te2Max.Name = "te2Max";
            this.te2Max.Size = new System.Drawing.Size(45, 20);
            this.te2Max.TabIndex = 20;
            this.te2Max.Value = new decimal(new int[] {
            372,
            0,
            0,
            65536});
            // 
            // te2Min
            // 
            this.te2Min.DecimalPlaces = 1;
            this.te2Min.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te2Min.Location = new System.Drawing.Point(73, 128);
            this.te2Min.Name = "te2Min";
            this.te2Min.Size = new System.Drawing.Size(45, 20);
            this.te2Min.TabIndex = 19;
            this.te2Min.Value = new decimal(new int[] {
            365,
            0,
            0,
            65536});
            // 
            // pr2Max
            // 
            this.pr2Max.Location = new System.Drawing.Point(124, 85);
            this.pr2Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.pr2Max.Name = "pr2Max";
            this.pr2Max.Size = new System.Drawing.Size(45, 20);
            this.pr2Max.TabIndex = 18;
            this.pr2Max.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pr2Min
            // 
            this.pr2Min.Location = new System.Drawing.Point(73, 85);
            this.pr2Min.Name = "pr2Min";
            this.pr2Min.Size = new System.Drawing.Size(45, 20);
            this.pr2Min.TabIndex = 17;
            this.pr2Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // br2Max
            // 
            this.br2Max.Location = new System.Drawing.Point(124, 44);
            this.br2Max.Name = "br2Max";
            this.br2Max.Size = new System.Drawing.Size(45, 20);
            this.br2Max.TabIndex = 16;
            this.br2Max.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // br2Min
            // 
            this.br2Min.Location = new System.Drawing.Point(73, 44);
            this.br2Min.Name = "br2Min";
            this.br2Min.Size = new System.Drawing.Size(45, 20);
            this.br2Min.TabIndex = 15;
            this.br2Min.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // p2
            // 
            this.p2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("p2.BackgroundImage")));
            this.p2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.p2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.p2.Controls.Add(this.l2re);
            this.p2.Controls.Add(this.bp2s);
            this.p2.Controls.Add(this.label1);
            this.p2.Controls.Add(this.bp2d);
            this.p2.Controls.Add(this.label2);
            this.p2.Controls.Add(this.te2s);
            this.p2.Controls.Add(this.label3);
            this.p2.Controls.Add(this.te2d);
            this.p2.Controls.Add(this.l2Bpd);
            this.p2.Controls.Add(this.pr2s);
            this.p2.Controls.Add(this.l2Bps);
            this.p2.Controls.Add(this.pr2d);
            this.p2.Controls.Add(this.l2Te);
            this.p2.Controls.Add(this.br2s);
            this.p2.Controls.Add(this.br2d);
            this.p2.Controls.Add(this.l2Pr);
            this.p2.Controls.Add(this.l2Br);
            this.p2.Controls.Add(this.m2);
            this.p2.Controls.Add(this.checkBox2);
            this.p2.Controls.Add(this.label21);
            this.p2.Controls.Add(this.label22);
            this.p2.Controls.Add(this.label23);
            this.p2.Controls.Add(this.label24);
            this.p2.Controls.Add(this.label25);
            this.p2.Controls.Add(this.label26);
            this.p2.Controls.Add(this.label27);
            this.p2.Controls.Add(this.bpd2Max);
            this.p2.Controls.Add(this.bpd2Min);
            this.p2.Controls.Add(this.bps2Max);
            this.p2.Controls.Add(this.bps2Min);
            this.p2.Controls.Add(this.te2Max);
            this.p2.Controls.Add(this.te2Min);
            this.p2.Controls.Add(this.pr2Max);
            this.p2.Controls.Add(this.pr2Min);
            this.p2.Controls.Add(this.br2Max);
            this.p2.Controls.Add(this.br2Min);
            this.p2.Location = new System.Drawing.Point(319, 33);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(259, 240);
            this.p2.TabIndex = 49;
            // 
            // l2re
            // 
            this.l2re.Location = new System.Drawing.Point(206, 213);
            this.l2re.Name = "l2re";
            this.l2re.Size = new System.Drawing.Size(44, 22);
            this.l2re.TabIndex = 50;
            this.l2re.Text = "Reset";
            this.l2re.UseVisualStyleBackColor = true;
            this.l2re.Click += new System.EventHandler(this.l2Re_Click);
            // 
            // bp2s
            // 
            this.bp2s.AutoSize = true;
            this.bp2s.Location = new System.Drawing.Point(217, 176);
            this.bp2s.Name = "bp2s";
            this.bp2s.Size = new System.Drawing.Size(34, 13);
            this.bp2s.TabIndex = 70;
            this.bp2s.Text = "Selec";
            this.bp2s.Click += new System.EventHandler(this.bp2s_Click);
            // 
            // bp2d
            // 
            this.bp2d.AutoSize = true;
            this.bp2d.Location = new System.Drawing.Point(203, 176);
            this.bp2d.Name = "bp2d";
            this.bp2d.Size = new System.Drawing.Size(54, 13);
            this.bp2d.TabIndex = 69;
            this.bp2d.Text = "De Select";
            this.bp2d.Click += new System.EventHandler(this.bp2d_Click);
            // 
            // te2s
            // 
            this.te2s.AutoSize = true;
            this.te2s.Location = new System.Drawing.Point(217, 129);
            this.te2s.Name = "te2s";
            this.te2s.Size = new System.Drawing.Size(34, 13);
            this.te2s.TabIndex = 68;
            this.te2s.Text = "Selec";
            this.te2s.Click += new System.EventHandler(this.te2s_Click);
            // 
            // te2d
            // 
            this.te2d.AutoSize = true;
            this.te2d.Location = new System.Drawing.Point(203, 129);
            this.te2d.Name = "te2d";
            this.te2d.Size = new System.Drawing.Size(54, 13);
            this.te2d.TabIndex = 67;
            this.te2d.Text = "De Select";
            this.te2d.Click += new System.EventHandler(this.te2d_Click);
            // 
            // pr2s
            // 
            this.pr2s.AutoSize = true;
            this.pr2s.Location = new System.Drawing.Point(217, 87);
            this.pr2s.Name = "pr2s";
            this.pr2s.Size = new System.Drawing.Size(34, 13);
            this.pr2s.TabIndex = 66;
            this.pr2s.Text = "Selec";
            this.pr2s.Click += new System.EventHandler(this.pr2s_Click);
            // 
            // pr2d
            // 
            this.pr2d.AutoSize = true;
            this.pr2d.Location = new System.Drawing.Point(203, 87);
            this.pr2d.Name = "pr2d";
            this.pr2d.Size = new System.Drawing.Size(54, 13);
            this.pr2d.TabIndex = 65;
            this.pr2d.Text = "De Select";
            this.pr2d.Click += new System.EventHandler(this.pr2d_Click);
            // 
            // br2s
            // 
            this.br2s.AutoSize = true;
            this.br2s.Location = new System.Drawing.Point(217, 46);
            this.br2s.Name = "br2s";
            this.br2s.Size = new System.Drawing.Size(34, 13);
            this.br2s.TabIndex = 64;
            this.br2s.Text = "Selec";
            this.br2s.Click += new System.EventHandler(this.br2s_Click);
            // 
            // br2d
            // 
            this.br2d.AutoSize = true;
            this.br2d.Location = new System.Drawing.Point(203, 46);
            this.br2d.Name = "br2d";
            this.br2d.Size = new System.Drawing.Size(54, 13);
            this.br2d.TabIndex = 63;
            this.br2d.Text = "De Select";
            this.br2d.Click += new System.EventHandler(this.br2d_Click);
            // 
            // p3
            // 
            this.p3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("p3.BackgroundImage")));
            this.p3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.p3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.p3.Controls.Add(this.l3re);
            this.p3.Controls.Add(this.bp3s);
            this.p3.Controls.Add(this.label4);
            this.p3.Controls.Add(this.bp3d);
            this.p3.Controls.Add(this.label5);
            this.p3.Controls.Add(this.te3s);
            this.p3.Controls.Add(this.label17);
            this.p3.Controls.Add(this.te3d);
            this.p3.Controls.Add(this.l3Bpd);
            this.p3.Controls.Add(this.pr3s);
            this.p3.Controls.Add(this.pr3d);
            this.p3.Controls.Add(this.l3Bps);
            this.p3.Controls.Add(this.br3s);
            this.p3.Controls.Add(this.l3Te);
            this.p3.Controls.Add(this.br3d);
            this.p3.Controls.Add(this.l3Pr);
            this.p3.Controls.Add(this.l3Br);
            this.p3.Controls.Add(this.label31);
            this.p3.Controls.Add(this.m3);
            this.p3.Controls.Add(this.label32);
            this.p3.Controls.Add(this.label33);
            this.p3.Controls.Add(this.label34);
            this.p3.Controls.Add(this.label35);
            this.p3.Controls.Add(this.label36);
            this.p3.Controls.Add(this.label37);
            this.p3.Controls.Add(this.label38);
            this.p3.Controls.Add(this.bpd3Max);
            this.p3.Controls.Add(this.bpd3Min);
            this.p3.Controls.Add(this.bps3Max);
            this.p3.Controls.Add(this.bps3Min);
            this.p3.Controls.Add(this.te3Max);
            this.p3.Controls.Add(this.te3Min);
            this.p3.Controls.Add(this.pr3Max);
            this.p3.Controls.Add(this.pr3Min);
            this.p3.Controls.Add(this.br3Max);
            this.p3.Controls.Add(this.br3Min);
            this.p3.Location = new System.Drawing.Point(628, 33);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(259, 240);
            this.p3.TabIndex = 50;
            // 
            // l3re
            // 
            this.l3re.Location = new System.Drawing.Point(205, 212);
            this.l3re.Name = "l3re";
            this.l3re.Size = new System.Drawing.Size(44, 22);
            this.l3re.TabIndex = 51;
            this.l3re.Text = "Reset";
            this.l3re.UseVisualStyleBackColor = true;
            this.l3re.Click += new System.EventHandler(this.l3Re_Click);
            // 
            // bp3s
            // 
            this.bp3s.AutoSize = true;
            this.bp3s.Location = new System.Drawing.Point(216, 176);
            this.bp3s.Name = "bp3s";
            this.bp3s.Size = new System.Drawing.Size(34, 13);
            this.bp3s.TabIndex = 80;
            this.bp3s.Text = "Selec";
            this.bp3s.Click += new System.EventHandler(this.bp3s_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 47;
            this.label4.Text = "Rate";
            // 
            // bp3d
            // 
            this.bp3d.AutoSize = true;
            this.bp3d.Location = new System.Drawing.Point(202, 176);
            this.bp3d.Name = "bp3d";
            this.bp3d.Size = new System.Drawing.Size(54, 13);
            this.bp3d.TabIndex = 79;
            this.bp3d.Text = "De Select";
            this.bp3d.Click += new System.EventHandler(this.bp3d_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "Rate";
            // 
            // te3s
            // 
            this.te3s.AutoSize = true;
            this.te3s.Location = new System.Drawing.Point(216, 129);
            this.te3s.Name = "te3s";
            this.te3s.Size = new System.Drawing.Size(34, 13);
            this.te3s.TabIndex = 78;
            this.te3s.Text = "Selec";
            this.te3s.Click += new System.EventHandler(this.te3s_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(173, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "Current";
            // 
            // te3d
            // 
            this.te3d.AutoSize = true;
            this.te3d.Location = new System.Drawing.Point(202, 129);
            this.te3d.Name = "te3d";
            this.te3d.Size = new System.Drawing.Size(54, 13);
            this.te3d.TabIndex = 77;
            this.te3d.Text = "De Select";
            this.te3d.Click += new System.EventHandler(this.te3d_Click);
            // 
            // l3Bpd
            // 
            this.l3Bpd.AutoSize = true;
            this.l3Bpd.Location = new System.Drawing.Point(176, 217);
            this.l3Bpd.Name = "l3Bpd";
            this.l3Bpd.Size = new System.Drawing.Size(19, 13);
            this.l3Bpd.TabIndex = 44;
            this.l3Bpd.Text = "70";
            // 
            // pr3s
            // 
            this.pr3s.AutoSize = true;
            this.pr3s.Location = new System.Drawing.Point(216, 87);
            this.pr3s.Name = "pr3s";
            this.pr3s.Size = new System.Drawing.Size(34, 13);
            this.pr3s.TabIndex = 76;
            this.pr3s.Text = "Selec";
            this.pr3s.Click += new System.EventHandler(this.pr3s_Click);
            // 
            // pr3d
            // 
            this.pr3d.AutoSize = true;
            this.pr3d.Location = new System.Drawing.Point(202, 87);
            this.pr3d.Name = "pr3d";
            this.pr3d.Size = new System.Drawing.Size(54, 13);
            this.pr3d.TabIndex = 75;
            this.pr3d.Text = "De Select";
            this.pr3d.Click += new System.EventHandler(this.pr3d_Click);
            // 
            // l3Bps
            // 
            this.l3Bps.AutoSize = true;
            this.l3Bps.Location = new System.Drawing.Point(176, 175);
            this.l3Bps.Name = "l3Bps";
            this.l3Bps.Size = new System.Drawing.Size(25, 13);
            this.l3Bps.TabIndex = 43;
            this.l3Bps.Text = "105";
            // 
            // br3s
            // 
            this.br3s.AutoSize = true;
            this.br3s.Location = new System.Drawing.Point(216, 46);
            this.br3s.Name = "br3s";
            this.br3s.Size = new System.Drawing.Size(34, 13);
            this.br3s.TabIndex = 74;
            this.br3s.Text = "Selec";
            this.br3s.Click += new System.EventHandler(this.br3s_Click);
            // 
            // l3Te
            // 
            this.l3Te.AutoSize = true;
            this.l3Te.Location = new System.Drawing.Point(176, 130);
            this.l3Te.Name = "l3Te";
            this.l3Te.Size = new System.Drawing.Size(28, 13);
            this.l3Te.TabIndex = 42;
            this.l3Te.Text = "36.9";
            // 
            // br3d
            // 
            this.br3d.AutoSize = true;
            this.br3d.Location = new System.Drawing.Point(202, 46);
            this.br3d.Name = "br3d";
            this.br3d.Size = new System.Drawing.Size(54, 13);
            this.br3d.TabIndex = 73;
            this.br3d.Text = "De Select";
            this.br3d.Click += new System.EventHandler(this.br3d_Click);
            // 
            // l3Pr
            // 
            this.l3Pr.AutoSize = true;
            this.l3Pr.Location = new System.Drawing.Point(176, 87);
            this.l3Pr.Name = "l3Pr";
            this.l3Pr.Size = new System.Drawing.Size(19, 13);
            this.l3Pr.TabIndex = 41;
            this.l3Pr.Text = "80";
            // 
            // l3Br
            // 
            this.l3Br.AutoSize = true;
            this.l3Br.Location = new System.Drawing.Point(176, 46);
            this.l3Br.Name = "l3Br";
            this.l3Br.Size = new System.Drawing.Size(19, 13);
            this.l3Br.TabIndex = 40;
            this.l3Br.Text = "16";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(218, 3);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 13);
            this.label31.TabIndex = 39;
            this.label31.Text = "Bed 3";
            // 
            // m3
            // 
            this.m3.AutoSize = true;
            this.m3.Location = new System.Drawing.Point(11, 8);
            this.m3.Name = "m3";
            this.m3.Size = new System.Drawing.Size(50, 17);
            this.m3.TabIndex = 38;
            this.m3.Text = "Mute";
            this.m3.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(71, 28);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(24, 13);
            this.label32.TabIndex = 36;
            this.label32.Text = "Min";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(122, 28);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(27, 13);
            this.label33.TabIndex = 35;
            this.label33.Text = "Max";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(9, 217);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(64, 13);
            this.label34.TabIndex = 34;
            this.label34.Text = "BP Diastolic";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(9, 175);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(60, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "BP Systolic";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(5, 130);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(67, 13);
            this.label36.TabIndex = 32;
            this.label36.Text = "Temperature";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(17, 80);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(33, 13);
            this.label37.TabIndex = 31;
            this.label37.Text = "Pulse";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(9, 40);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(52, 13);
            this.label38.TabIndex = 30;
            this.label38.Text = "Breathing";
            // 
            // bpd3Max
            // 
            this.bpd3Max.Location = new System.Drawing.Point(125, 215);
            this.bpd3Max.Name = "bpd3Max";
            this.bpd3Max.Size = new System.Drawing.Size(45, 20);
            this.bpd3Max.TabIndex = 24;
            this.bpd3Max.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // bpd3Min
            // 
            this.bpd3Min.Location = new System.Drawing.Point(74, 215);
            this.bpd3Min.Name = "bpd3Min";
            this.bpd3Min.Size = new System.Drawing.Size(45, 20);
            this.bpd3Min.TabIndex = 23;
            this.bpd3Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // bps3Max
            // 
            this.bps3Max.Location = new System.Drawing.Point(125, 173);
            this.bps3Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.bps3Max.Name = "bps3Max";
            this.bps3Max.Size = new System.Drawing.Size(45, 20);
            this.bps3Max.TabIndex = 22;
            this.bps3Max.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // bps3Min
            // 
            this.bps3Min.Location = new System.Drawing.Point(74, 173);
            this.bps3Min.Name = "bps3Min";
            this.bps3Min.Size = new System.Drawing.Size(45, 20);
            this.bps3Min.TabIndex = 21;
            this.bps3Min.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // te3Max
            // 
            this.te3Max.DecimalPlaces = 1;
            this.te3Max.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te3Max.Location = new System.Drawing.Point(125, 128);
            this.te3Max.Name = "te3Max";
            this.te3Max.Size = new System.Drawing.Size(45, 20);
            this.te3Max.TabIndex = 20;
            this.te3Max.Value = new decimal(new int[] {
            372,
            0,
            0,
            65536});
            // 
            // te3Min
            // 
            this.te3Min.DecimalPlaces = 1;
            this.te3Min.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te3Min.Location = new System.Drawing.Point(74, 128);
            this.te3Min.Name = "te3Min";
            this.te3Min.Size = new System.Drawing.Size(45, 20);
            this.te3Min.TabIndex = 19;
            this.te3Min.Value = new decimal(new int[] {
            365,
            0,
            0,
            65536});
            // 
            // pr3Max
            // 
            this.pr3Max.Location = new System.Drawing.Point(125, 85);
            this.pr3Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.pr3Max.Name = "pr3Max";
            this.pr3Max.Size = new System.Drawing.Size(45, 20);
            this.pr3Max.TabIndex = 18;
            this.pr3Max.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pr3Min
            // 
            this.pr3Min.Location = new System.Drawing.Point(74, 85);
            this.pr3Min.Name = "pr3Min";
            this.pr3Min.Size = new System.Drawing.Size(45, 20);
            this.pr3Min.TabIndex = 17;
            this.pr3Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // br3Max
            // 
            this.br3Max.Location = new System.Drawing.Point(125, 44);
            this.br3Max.Name = "br3Max";
            this.br3Max.Size = new System.Drawing.Size(45, 20);
            this.br3Max.TabIndex = 16;
            this.br3Max.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // br3Min
            // 
            this.br3Min.Location = new System.Drawing.Point(74, 44);
            this.br3Min.Name = "br3Min";
            this.br3Min.Size = new System.Drawing.Size(45, 20);
            this.br3Min.TabIndex = 15;
            this.br3Min.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // p4
            // 
            this.p4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("p4.BackgroundImage")));
            this.p4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.p4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.p4.Controls.Add(this.l4re);
            this.p4.Controls.Add(this.bp4s);
            this.p4.Controls.Add(this.label39);
            this.p4.Controls.Add(this.bp4d);
            this.p4.Controls.Add(this.label40);
            this.p4.Controls.Add(this.te4s);
            this.p4.Controls.Add(this.label41);
            this.p4.Controls.Add(this.te4d);
            this.p4.Controls.Add(this.l4Bpd);
            this.p4.Controls.Add(this.pr4s);
            this.p4.Controls.Add(this.pr4d);
            this.p4.Controls.Add(this.l4Bps);
            this.p4.Controls.Add(this.br4s);
            this.p4.Controls.Add(this.l4Te);
            this.p4.Controls.Add(this.br4d);
            this.p4.Controls.Add(this.l4Pr);
            this.p4.Controls.Add(this.l4Br);
            this.p4.Controls.Add(this.label47);
            this.p4.Controls.Add(this.m4);
            this.p4.Controls.Add(this.label48);
            this.p4.Controls.Add(this.label49);
            this.p4.Controls.Add(this.label50);
            this.p4.Controls.Add(this.label51);
            this.p4.Controls.Add(this.label52);
            this.p4.Controls.Add(this.label53);
            this.p4.Controls.Add(this.label54);
            this.p4.Controls.Add(this.bpd4Max);
            this.p4.Controls.Add(this.bpd4Min);
            this.p4.Controls.Add(this.bps4Max);
            this.p4.Controls.Add(this.bps4Min);
            this.p4.Controls.Add(this.te4Max);
            this.p4.Controls.Add(this.te4Min);
            this.p4.Controls.Add(this.pr4Max);
            this.p4.Controls.Add(this.pr4Min);
            this.p4.Controls.Add(this.br4Max);
            this.p4.Controls.Add(this.br4Min);
            this.p4.Location = new System.Drawing.Point(938, 33);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(259, 240);
            this.p4.TabIndex = 50;
            // 
            // l4re
            // 
            this.l4re.Location = new System.Drawing.Point(207, 212);
            this.l4re.Name = "l4re";
            this.l4re.Size = new System.Drawing.Size(44, 22);
            this.l4re.TabIndex = 52;
            this.l4re.Text = "Reset";
            this.l4re.UseVisualStyleBackColor = true;
            this.l4re.Click += new System.EventHandler(this.l4Re_Click);
            // 
            // bp4s
            // 
            this.bp4s.AutoSize = true;
            this.bp4s.Location = new System.Drawing.Point(218, 176);
            this.bp4s.Name = "bp4s";
            this.bp4s.Size = new System.Drawing.Size(34, 13);
            this.bp4s.TabIndex = 90;
            this.bp4s.Text = "Selec";
            this.bp4s.Click += new System.EventHandler(this.bp4s_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(17, 93);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(30, 13);
            this.label39.TabIndex = 47;
            this.label39.Text = "Rate";
            // 
            // bp4d
            // 
            this.bp4d.AutoSize = true;
            this.bp4d.Location = new System.Drawing.Point(204, 176);
            this.bp4d.Name = "bp4d";
            this.bp4d.Size = new System.Drawing.Size(54, 13);
            this.bp4d.TabIndex = 89;
            this.bp4d.Text = "De Select";
            this.bp4d.Click += new System.EventHandler(this.bp4d_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(20, 53);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(30, 13);
            this.label40.TabIndex = 46;
            this.label40.Text = "Rate";
            // 
            // te4s
            // 
            this.te4s.AutoSize = true;
            this.te4s.Location = new System.Drawing.Point(218, 129);
            this.te4s.Name = "te4s";
            this.te4s.Size = new System.Drawing.Size(34, 13);
            this.te4s.TabIndex = 88;
            this.te4s.Text = "Selec";
            this.te4s.Click += new System.EventHandler(this.te4s_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(173, 28);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(41, 13);
            this.label41.TabIndex = 45;
            this.label41.Text = "Current";
            // 
            // te4d
            // 
            this.te4d.AutoSize = true;
            this.te4d.Location = new System.Drawing.Point(204, 129);
            this.te4d.Name = "te4d";
            this.te4d.Size = new System.Drawing.Size(54, 13);
            this.te4d.TabIndex = 87;
            this.te4d.Text = "De Select";
            this.te4d.Click += new System.EventHandler(this.te4d_Click);
            // 
            // l4Bpd
            // 
            this.l4Bpd.AutoSize = true;
            this.l4Bpd.Location = new System.Drawing.Point(176, 217);
            this.l4Bpd.Name = "l4Bpd";
            this.l4Bpd.Size = new System.Drawing.Size(19, 13);
            this.l4Bpd.TabIndex = 44;
            this.l4Bpd.Text = "70";
            // 
            // pr4s
            // 
            this.pr4s.AutoSize = true;
            this.pr4s.Location = new System.Drawing.Point(218, 87);
            this.pr4s.Name = "pr4s";
            this.pr4s.Size = new System.Drawing.Size(34, 13);
            this.pr4s.TabIndex = 86;
            this.pr4s.Text = "Selec";
            this.pr4s.Click += new System.EventHandler(this.pr4s_Click);
            // 
            // pr4d
            // 
            this.pr4d.AutoSize = true;
            this.pr4d.Location = new System.Drawing.Point(204, 87);
            this.pr4d.Name = "pr4d";
            this.pr4d.Size = new System.Drawing.Size(54, 13);
            this.pr4d.TabIndex = 85;
            this.pr4d.Text = "De Select";
            this.pr4d.Click += new System.EventHandler(this.pr4d_Click);
            // 
            // l4Bps
            // 
            this.l4Bps.AutoSize = true;
            this.l4Bps.Location = new System.Drawing.Point(176, 175);
            this.l4Bps.Name = "l4Bps";
            this.l4Bps.Size = new System.Drawing.Size(25, 13);
            this.l4Bps.TabIndex = 43;
            this.l4Bps.Text = "105";
            // 
            // br4s
            // 
            this.br4s.AutoSize = true;
            this.br4s.Location = new System.Drawing.Point(218, 46);
            this.br4s.Name = "br4s";
            this.br4s.Size = new System.Drawing.Size(34, 13);
            this.br4s.TabIndex = 84;
            this.br4s.Text = "Selec";
            this.br4s.Click += new System.EventHandler(this.br4s_Click);
            // 
            // l4Te
            // 
            this.l4Te.AutoSize = true;
            this.l4Te.Location = new System.Drawing.Point(176, 130);
            this.l4Te.Name = "l4Te";
            this.l4Te.Size = new System.Drawing.Size(28, 13);
            this.l4Te.TabIndex = 42;
            this.l4Te.Text = "36.9";
            // 
            // br4d
            // 
            this.br4d.AutoSize = true;
            this.br4d.Location = new System.Drawing.Point(204, 46);
            this.br4d.Name = "br4d";
            this.br4d.Size = new System.Drawing.Size(54, 13);
            this.br4d.TabIndex = 83;
            this.br4d.Text = "De Select";
            this.br4d.Click += new System.EventHandler(this.br4d_Click);
            // 
            // l4Pr
            // 
            this.l4Pr.AutoSize = true;
            this.l4Pr.Location = new System.Drawing.Point(176, 87);
            this.l4Pr.Name = "l4Pr";
            this.l4Pr.Size = new System.Drawing.Size(19, 13);
            this.l4Pr.TabIndex = 41;
            this.l4Pr.Text = "80";
            // 
            // l4Br
            // 
            this.l4Br.AutoSize = true;
            this.l4Br.Location = new System.Drawing.Point(176, 46);
            this.l4Br.Name = "l4Br";
            this.l4Br.Size = new System.Drawing.Size(19, 13);
            this.l4Br.TabIndex = 40;
            this.l4Br.Text = "16";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(217, 3);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(35, 13);
            this.label47.TabIndex = 39;
            this.label47.Text = "Bed 4";
            // 
            // m4
            // 
            this.m4.AutoSize = true;
            this.m4.Location = new System.Drawing.Point(11, 8);
            this.m4.Name = "m4";
            this.m4.Size = new System.Drawing.Size(50, 17);
            this.m4.TabIndex = 38;
            this.m4.Text = "Mute";
            this.m4.UseVisualStyleBackColor = true;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(71, 28);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(24, 13);
            this.label48.TabIndex = 36;
            this.label48.Text = "Min";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(122, 28);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(27, 13);
            this.label49.TabIndex = 35;
            this.label49.Text = "Max";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(9, 217);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(64, 13);
            this.label50.TabIndex = 34;
            this.label50.Text = "BP Diastolic";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(9, 175);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(60, 13);
            this.label51.TabIndex = 33;
            this.label51.Text = "BP Systolic";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(5, 130);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(67, 13);
            this.label52.TabIndex = 32;
            this.label52.Text = "Temperature";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(17, 80);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(33, 13);
            this.label53.TabIndex = 31;
            this.label53.Text = "Pulse";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(9, 40);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(52, 13);
            this.label54.TabIndex = 30;
            this.label54.Text = "Breathing";
            // 
            // bpd4Max
            // 
            this.bpd4Max.Location = new System.Drawing.Point(125, 215);
            this.bpd4Max.Name = "bpd4Max";
            this.bpd4Max.Size = new System.Drawing.Size(45, 20);
            this.bpd4Max.TabIndex = 24;
            this.bpd4Max.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // bpd4Min
            // 
            this.bpd4Min.Location = new System.Drawing.Point(74, 215);
            this.bpd4Min.Name = "bpd4Min";
            this.bpd4Min.Size = new System.Drawing.Size(45, 20);
            this.bpd4Min.TabIndex = 23;
            this.bpd4Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // bps4Max
            // 
            this.bps4Max.Location = new System.Drawing.Point(125, 173);
            this.bps4Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.bps4Max.Name = "bps4Max";
            this.bps4Max.Size = new System.Drawing.Size(45, 20);
            this.bps4Max.TabIndex = 22;
            this.bps4Max.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // bps4Min
            // 
            this.bps4Min.Location = new System.Drawing.Point(74, 173);
            this.bps4Min.Name = "bps4Min";
            this.bps4Min.Size = new System.Drawing.Size(45, 20);
            this.bps4Min.TabIndex = 21;
            this.bps4Min.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // te4Max
            // 
            this.te4Max.DecimalPlaces = 1;
            this.te4Max.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te4Max.Location = new System.Drawing.Point(125, 128);
            this.te4Max.Name = "te4Max";
            this.te4Max.Size = new System.Drawing.Size(45, 20);
            this.te4Max.TabIndex = 20;
            this.te4Max.Value = new decimal(new int[] {
            372,
            0,
            0,
            65536});
            // 
            // te4Min
            // 
            this.te4Min.DecimalPlaces = 1;
            this.te4Min.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te4Min.Location = new System.Drawing.Point(74, 128);
            this.te4Min.Name = "te4Min";
            this.te4Min.Size = new System.Drawing.Size(45, 20);
            this.te4Min.TabIndex = 19;
            this.te4Min.Value = new decimal(new int[] {
            365,
            0,
            0,
            65536});
            // 
            // pr4Max
            // 
            this.pr4Max.Location = new System.Drawing.Point(125, 85);
            this.pr4Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.pr4Max.Name = "pr4Max";
            this.pr4Max.Size = new System.Drawing.Size(45, 20);
            this.pr4Max.TabIndex = 18;
            this.pr4Max.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pr4Min
            // 
            this.pr4Min.Location = new System.Drawing.Point(74, 85);
            this.pr4Min.Name = "pr4Min";
            this.pr4Min.Size = new System.Drawing.Size(45, 20);
            this.pr4Min.TabIndex = 17;
            this.pr4Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // br4Max
            // 
            this.br4Max.Location = new System.Drawing.Point(125, 44);
            this.br4Max.Name = "br4Max";
            this.br4Max.Size = new System.Drawing.Size(45, 20);
            this.br4Max.TabIndex = 16;
            this.br4Max.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // br4Min
            // 
            this.br4Min.Location = new System.Drawing.Point(74, 44);
            this.br4Min.Name = "br4Min";
            this.br4Min.Size = new System.Drawing.Size(45, 20);
            this.br4Min.TabIndex = 15;
            this.br4Min.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // p5
            // 
            this.p5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("p5.BackgroundImage")));
            this.p5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.p5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.p5.Controls.Add(this.l5re);
            this.p5.Controls.Add(this.bp5s);
            this.p5.Controls.Add(this.label55);
            this.p5.Controls.Add(this.bp5d);
            this.p5.Controls.Add(this.label56);
            this.p5.Controls.Add(this.te5s);
            this.p5.Controls.Add(this.label57);
            this.p5.Controls.Add(this.te5d);
            this.p5.Controls.Add(this.l5Bpd);
            this.p5.Controls.Add(this.pr5s);
            this.p5.Controls.Add(this.pr5d);
            this.p5.Controls.Add(this.l5Bps);
            this.p5.Controls.Add(this.br5s);
            this.p5.Controls.Add(this.l5Te);
            this.p5.Controls.Add(this.br5d);
            this.p5.Controls.Add(this.l5Pr);
            this.p5.Controls.Add(this.l5Br);
            this.p5.Controls.Add(this.label63);
            this.p5.Controls.Add(this.m5);
            this.p5.Controls.Add(this.label64);
            this.p5.Controls.Add(this.label65);
            this.p5.Controls.Add(this.label66);
            this.p5.Controls.Add(this.label67);
            this.p5.Controls.Add(this.label68);
            this.p5.Controls.Add(this.label69);
            this.p5.Controls.Add(this.label70);
            this.p5.Controls.Add(this.bpd5Max);
            this.p5.Controls.Add(this.bpd5Min);
            this.p5.Controls.Add(this.bps5Max);
            this.p5.Controls.Add(this.bps5Min);
            this.p5.Controls.Add(this.te5Max);
            this.p5.Controls.Add(this.te5Min);
            this.p5.Controls.Add(this.pr5Max);
            this.p5.Controls.Add(this.pr5Min);
            this.p5.Controls.Add(this.br5Max);
            this.p5.Controls.Add(this.br5Min);
            this.p5.Location = new System.Drawing.Point(12, 314);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(259, 240);
            this.p5.TabIndex = 50;
            // 
            // l5re
            // 
            this.l5re.Location = new System.Drawing.Point(205, 212);
            this.l5re.Name = "l5re";
            this.l5re.Size = new System.Drawing.Size(44, 22);
            this.l5re.TabIndex = 52;
            this.l5re.Text = "Reset";
            this.l5re.UseVisualStyleBackColor = true;
            this.l5re.Click += new System.EventHandler(this.l5Re_Click);
            // 
            // bp5s
            // 
            this.bp5s.AutoSize = true;
            this.bp5s.Location = new System.Drawing.Point(216, 176);
            this.bp5s.Name = "bp5s";
            this.bp5s.Size = new System.Drawing.Size(34, 13);
            this.bp5s.TabIndex = 80;
            this.bp5s.Text = "Selec";
            this.bp5s.Click += new System.EventHandler(this.bp5s_Click);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(16, 93);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(30, 13);
            this.label55.TabIndex = 47;
            this.label55.Text = "Rate";
            // 
            // bp5d
            // 
            this.bp5d.AutoSize = true;
            this.bp5d.Location = new System.Drawing.Point(202, 176);
            this.bp5d.Name = "bp5d";
            this.bp5d.Size = new System.Drawing.Size(54, 13);
            this.bp5d.TabIndex = 79;
            this.bp5d.Text = "De Select";
            this.bp5d.Click += new System.EventHandler(this.bp5d_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(19, 53);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(30, 13);
            this.label56.TabIndex = 46;
            this.label56.Text = "Rate";
            // 
            // te5s
            // 
            this.te5s.AutoSize = true;
            this.te5s.Location = new System.Drawing.Point(216, 129);
            this.te5s.Name = "te5s";
            this.te5s.Size = new System.Drawing.Size(34, 13);
            this.te5s.TabIndex = 78;
            this.te5s.Text = "Selec";
            this.te5s.Click += new System.EventHandler(this.te5s_Click);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(172, 28);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(41, 13);
            this.label57.TabIndex = 45;
            this.label57.Text = "Current";
            // 
            // te5d
            // 
            this.te5d.AutoSize = true;
            this.te5d.Location = new System.Drawing.Point(202, 129);
            this.te5d.Name = "te5d";
            this.te5d.Size = new System.Drawing.Size(54, 13);
            this.te5d.TabIndex = 77;
            this.te5d.Text = "De Select";
            this.te5d.Click += new System.EventHandler(this.te5d_Click);
            // 
            // l5Bpd
            // 
            this.l5Bpd.AutoSize = true;
            this.l5Bpd.Location = new System.Drawing.Point(175, 217);
            this.l5Bpd.Name = "l5Bpd";
            this.l5Bpd.Size = new System.Drawing.Size(19, 13);
            this.l5Bpd.TabIndex = 44;
            this.l5Bpd.Text = "70";
            // 
            // pr5s
            // 
            this.pr5s.AutoSize = true;
            this.pr5s.Location = new System.Drawing.Point(216, 87);
            this.pr5s.Name = "pr5s";
            this.pr5s.Size = new System.Drawing.Size(34, 13);
            this.pr5s.TabIndex = 76;
            this.pr5s.Text = "Selec";
            this.pr5s.Click += new System.EventHandler(this.pr5s_Click);
            // 
            // pr5d
            // 
            this.pr5d.AutoSize = true;
            this.pr5d.Location = new System.Drawing.Point(202, 87);
            this.pr5d.Name = "pr5d";
            this.pr5d.Size = new System.Drawing.Size(54, 13);
            this.pr5d.TabIndex = 75;
            this.pr5d.Text = "De Select";
            this.pr5d.Click += new System.EventHandler(this.pr5d_Click);
            // 
            // l5Bps
            // 
            this.l5Bps.AutoSize = true;
            this.l5Bps.Location = new System.Drawing.Point(175, 175);
            this.l5Bps.Name = "l5Bps";
            this.l5Bps.Size = new System.Drawing.Size(25, 13);
            this.l5Bps.TabIndex = 43;
            this.l5Bps.Text = "105";
            // 
            // br5s
            // 
            this.br5s.AutoSize = true;
            this.br5s.Location = new System.Drawing.Point(216, 46);
            this.br5s.Name = "br5s";
            this.br5s.Size = new System.Drawing.Size(34, 13);
            this.br5s.TabIndex = 74;
            this.br5s.Text = "Selec";
            this.br5s.Click += new System.EventHandler(this.br5s_Click);
            // 
            // l5Te
            // 
            this.l5Te.AutoSize = true;
            this.l5Te.Location = new System.Drawing.Point(175, 130);
            this.l5Te.Name = "l5Te";
            this.l5Te.Size = new System.Drawing.Size(28, 13);
            this.l5Te.TabIndex = 42;
            this.l5Te.Text = "36.9";
            // 
            // br5d
            // 
            this.br5d.AutoSize = true;
            this.br5d.Location = new System.Drawing.Point(202, 46);
            this.br5d.Name = "br5d";
            this.br5d.Size = new System.Drawing.Size(54, 13);
            this.br5d.TabIndex = 73;
            this.br5d.Text = "De Select";
            this.br5d.Click += new System.EventHandler(this.br5d_Click);
            // 
            // l5Pr
            // 
            this.l5Pr.AutoSize = true;
            this.l5Pr.Location = new System.Drawing.Point(175, 87);
            this.l5Pr.Name = "l5Pr";
            this.l5Pr.Size = new System.Drawing.Size(19, 13);
            this.l5Pr.TabIndex = 41;
            this.l5Pr.Text = "80";
            // 
            // l5Br
            // 
            this.l5Br.AutoSize = true;
            this.l5Br.Location = new System.Drawing.Point(175, 46);
            this.l5Br.Name = "l5Br";
            this.l5Br.Size = new System.Drawing.Size(19, 13);
            this.l5Br.TabIndex = 40;
            this.l5Br.Text = "16";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(215, 8);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(35, 13);
            this.label63.TabIndex = 39;
            this.label63.Text = "Bed 5";
            // 
            // m5
            // 
            this.m5.AutoSize = true;
            this.m5.Location = new System.Drawing.Point(10, 8);
            this.m5.Name = "m5";
            this.m5.Size = new System.Drawing.Size(50, 17);
            this.m5.TabIndex = 38;
            this.m5.Text = "Mute";
            this.m5.UseVisualStyleBackColor = true;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(70, 28);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(24, 13);
            this.label64.TabIndex = 36;
            this.label64.Text = "Min";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(121, 28);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(27, 13);
            this.label65.TabIndex = 35;
            this.label65.Text = "Max";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(8, 217);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(64, 13);
            this.label66.TabIndex = 34;
            this.label66.Text = "BP Diastolic";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(8, 175);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(60, 13);
            this.label67.TabIndex = 33;
            this.label67.Text = "BP Systolic";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(4, 130);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(67, 13);
            this.label68.TabIndex = 32;
            this.label68.Text = "Temperature";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(16, 80);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(33, 13);
            this.label69.TabIndex = 31;
            this.label69.Text = "Pulse";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(8, 40);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(52, 13);
            this.label70.TabIndex = 30;
            this.label70.Text = "Breathing";
            // 
            // bpd5Max
            // 
            this.bpd5Max.Location = new System.Drawing.Point(124, 215);
            this.bpd5Max.Name = "bpd5Max";
            this.bpd5Max.Size = new System.Drawing.Size(45, 20);
            this.bpd5Max.TabIndex = 24;
            this.bpd5Max.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // bpd5Min
            // 
            this.bpd5Min.Location = new System.Drawing.Point(73, 215);
            this.bpd5Min.Name = "bpd5Min";
            this.bpd5Min.Size = new System.Drawing.Size(45, 20);
            this.bpd5Min.TabIndex = 23;
            this.bpd5Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // bps5Max
            // 
            this.bps5Max.Location = new System.Drawing.Point(124, 173);
            this.bps5Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.bps5Max.Name = "bps5Max";
            this.bps5Max.Size = new System.Drawing.Size(45, 20);
            this.bps5Max.TabIndex = 22;
            this.bps5Max.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // bps5Min
            // 
            this.bps5Min.Location = new System.Drawing.Point(73, 173);
            this.bps5Min.Name = "bps5Min";
            this.bps5Min.Size = new System.Drawing.Size(45, 20);
            this.bps5Min.TabIndex = 21;
            this.bps5Min.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // te5Max
            // 
            this.te5Max.DecimalPlaces = 1;
            this.te5Max.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te5Max.Location = new System.Drawing.Point(124, 128);
            this.te5Max.Name = "te5Max";
            this.te5Max.Size = new System.Drawing.Size(45, 20);
            this.te5Max.TabIndex = 20;
            this.te5Max.Value = new decimal(new int[] {
            372,
            0,
            0,
            65536});
            // 
            // te5Min
            // 
            this.te5Min.DecimalPlaces = 1;
            this.te5Min.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te5Min.Location = new System.Drawing.Point(73, 128);
            this.te5Min.Name = "te5Min";
            this.te5Min.Size = new System.Drawing.Size(45, 20);
            this.te5Min.TabIndex = 19;
            this.te5Min.Value = new decimal(new int[] {
            365,
            0,
            0,
            65536});
            // 
            // pr5Max
            // 
            this.pr5Max.Location = new System.Drawing.Point(124, 85);
            this.pr5Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.pr5Max.Name = "pr5Max";
            this.pr5Max.Size = new System.Drawing.Size(45, 20);
            this.pr5Max.TabIndex = 18;
            this.pr5Max.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pr5Min
            // 
            this.pr5Min.Location = new System.Drawing.Point(73, 85);
            this.pr5Min.Name = "pr5Min";
            this.pr5Min.Size = new System.Drawing.Size(45, 20);
            this.pr5Min.TabIndex = 17;
            this.pr5Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // br5Max
            // 
            this.br5Max.Location = new System.Drawing.Point(124, 44);
            this.br5Max.Name = "br5Max";
            this.br5Max.Size = new System.Drawing.Size(45, 20);
            this.br5Max.TabIndex = 16;
            this.br5Max.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // br5Min
            // 
            this.br5Min.Location = new System.Drawing.Point(73, 44);
            this.br5Min.Name = "br5Min";
            this.br5Min.Size = new System.Drawing.Size(45, 20);
            this.br5Min.TabIndex = 15;
            this.br5Min.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // p6
            // 
            this.p6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("p6.BackgroundImage")));
            this.p6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.p6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.p6.Controls.Add(this.l6re);
            this.p6.Controls.Add(this.bp6s);
            this.p6.Controls.Add(this.label71);
            this.p6.Controls.Add(this.bp6d);
            this.p6.Controls.Add(this.label72);
            this.p6.Controls.Add(this.te6s);
            this.p6.Controls.Add(this.label73);
            this.p6.Controls.Add(this.te6d);
            this.p6.Controls.Add(this.l6Bpd);
            this.p6.Controls.Add(this.pr6s);
            this.p6.Controls.Add(this.l6Bps);
            this.p6.Controls.Add(this.pr6d);
            this.p6.Controls.Add(this.l6Te);
            this.p6.Controls.Add(this.br6s);
            this.p6.Controls.Add(this.br6d);
            this.p6.Controls.Add(this.l6Pr);
            this.p6.Controls.Add(this.l6Br);
            this.p6.Controls.Add(this.m6);
            this.p6.Controls.Add(this.checkBox6);
            this.p6.Controls.Add(this.label80);
            this.p6.Controls.Add(this.label81);
            this.p6.Controls.Add(this.label82);
            this.p6.Controls.Add(this.label83);
            this.p6.Controls.Add(this.label84);
            this.p6.Controls.Add(this.label85);
            this.p6.Controls.Add(this.label86);
            this.p6.Controls.Add(this.bpd6Max);
            this.p6.Controls.Add(this.bpd6Min);
            this.p6.Controls.Add(this.bps6Max);
            this.p6.Controls.Add(this.bps6Min);
            this.p6.Controls.Add(this.te6Max);
            this.p6.Controls.Add(this.te6Min);
            this.p6.Controls.Add(this.pr6Max);
            this.p6.Controls.Add(this.pr6Min);
            this.p6.Controls.Add(this.br6Max);
            this.p6.Controls.Add(this.br6Min);
            this.p6.Location = new System.Drawing.Point(319, 314);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(259, 240);
            this.p6.TabIndex = 50;
            // 
            // l6re
            // 
            this.l6re.Location = new System.Drawing.Point(206, 212);
            this.l6re.Name = "l6re";
            this.l6re.Size = new System.Drawing.Size(44, 22);
            this.l6re.TabIndex = 53;
            this.l6re.Text = "Reset";
            this.l6re.UseVisualStyleBackColor = true;
            this.l6re.Click += new System.EventHandler(this.l6Re_Click);
            // 
            // bp6s
            // 
            this.bp6s.AutoSize = true;
            this.bp6s.Location = new System.Drawing.Point(220, 176);
            this.bp6s.Name = "bp6s";
            this.bp6s.Size = new System.Drawing.Size(34, 13);
            this.bp6s.TabIndex = 90;
            this.bp6s.Text = "Selec";
            this.bp6s.Click += new System.EventHandler(this.bp6s_Click);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(15, 93);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(30, 13);
            this.label71.TabIndex = 47;
            this.label71.Text = "Rate";
            // 
            // bp6d
            // 
            this.bp6d.AutoSize = true;
            this.bp6d.Location = new System.Drawing.Point(206, 176);
            this.bp6d.Name = "bp6d";
            this.bp6d.Size = new System.Drawing.Size(54, 13);
            this.bp6d.TabIndex = 89;
            this.bp6d.Text = "De Select";
            this.bp6d.Click += new System.EventHandler(this.bp6d_Click);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(18, 53);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(30, 13);
            this.label72.TabIndex = 46;
            this.label72.Text = "Rate";
            // 
            // te6s
            // 
            this.te6s.AutoSize = true;
            this.te6s.Location = new System.Drawing.Point(220, 129);
            this.te6s.Name = "te6s";
            this.te6s.Size = new System.Drawing.Size(34, 13);
            this.te6s.TabIndex = 88;
            this.te6s.Text = "Selec";
            this.te6s.Click += new System.EventHandler(this.te6s_Click);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(171, 28);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(41, 13);
            this.label73.TabIndex = 45;
            this.label73.Text = "Current";
            // 
            // te6d
            // 
            this.te6d.AutoSize = true;
            this.te6d.Location = new System.Drawing.Point(206, 129);
            this.te6d.Name = "te6d";
            this.te6d.Size = new System.Drawing.Size(54, 13);
            this.te6d.TabIndex = 87;
            this.te6d.Text = "De Select";
            this.te6d.Click += new System.EventHandler(this.te6d_Click);
            // 
            // l6Bpd
            // 
            this.l6Bpd.AutoSize = true;
            this.l6Bpd.Location = new System.Drawing.Point(174, 217);
            this.l6Bpd.Name = "l6Bpd";
            this.l6Bpd.Size = new System.Drawing.Size(19, 13);
            this.l6Bpd.TabIndex = 44;
            this.l6Bpd.Text = "70";
            // 
            // pr6s
            // 
            this.pr6s.AutoSize = true;
            this.pr6s.Location = new System.Drawing.Point(220, 87);
            this.pr6s.Name = "pr6s";
            this.pr6s.Size = new System.Drawing.Size(34, 13);
            this.pr6s.TabIndex = 86;
            this.pr6s.Text = "Selec";
            this.pr6s.Click += new System.EventHandler(this.pr6s_Click);
            // 
            // l6Bps
            // 
            this.l6Bps.AutoSize = true;
            this.l6Bps.Location = new System.Drawing.Point(174, 175);
            this.l6Bps.Name = "l6Bps";
            this.l6Bps.Size = new System.Drawing.Size(25, 13);
            this.l6Bps.TabIndex = 43;
            this.l6Bps.Text = "105";
            // 
            // pr6d
            // 
            this.pr6d.AutoSize = true;
            this.pr6d.Location = new System.Drawing.Point(206, 87);
            this.pr6d.Name = "pr6d";
            this.pr6d.Size = new System.Drawing.Size(54, 13);
            this.pr6d.TabIndex = 85;
            this.pr6d.Text = "De Select";
            this.pr6d.Click += new System.EventHandler(this.pr6d_Click);
            // 
            // l6Te
            // 
            this.l6Te.AutoSize = true;
            this.l6Te.Location = new System.Drawing.Point(174, 130);
            this.l6Te.Name = "l6Te";
            this.l6Te.Size = new System.Drawing.Size(28, 13);
            this.l6Te.TabIndex = 42;
            this.l6Te.Text = "36.9";
            // 
            // br6s
            // 
            this.br6s.AutoSize = true;
            this.br6s.Location = new System.Drawing.Point(220, 46);
            this.br6s.Name = "br6s";
            this.br6s.Size = new System.Drawing.Size(34, 13);
            this.br6s.TabIndex = 84;
            this.br6s.Text = "Selec";
            this.br6s.Click += new System.EventHandler(this.br6s_Click);
            // 
            // br6d
            // 
            this.br6d.AutoSize = true;
            this.br6d.Location = new System.Drawing.Point(206, 46);
            this.br6d.Name = "br6d";
            this.br6d.Size = new System.Drawing.Size(54, 13);
            this.br6d.TabIndex = 83;
            this.br6d.Text = "De Select";
            this.br6d.Click += new System.EventHandler(this.br6d_Click);
            // 
            // l6Pr
            // 
            this.l6Pr.AutoSize = true;
            this.l6Pr.Location = new System.Drawing.Point(174, 87);
            this.l6Pr.Name = "l6Pr";
            this.l6Pr.Size = new System.Drawing.Size(19, 13);
            this.l6Pr.TabIndex = 41;
            this.l6Pr.Text = "80";
            // 
            // l6Br
            // 
            this.l6Br.AutoSize = true;
            this.l6Br.Location = new System.Drawing.Point(174, 46);
            this.l6Br.Name = "l6Br";
            this.l6Br.Size = new System.Drawing.Size(19, 13);
            this.l6Br.TabIndex = 40;
            this.l6Br.Text = "16";
            // 
            // m6
            // 
            this.m6.AutoSize = true;
            this.m6.Location = new System.Drawing.Point(222, 4);
            this.m6.Name = "m6";
            this.m6.Size = new System.Drawing.Size(35, 13);
            this.m6.TabIndex = 39;
            this.m6.Text = "Bed 6";
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(9, 8);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(50, 17);
            this.checkBox6.TabIndex = 38;
            this.checkBox6.Text = "Mute";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(69, 28);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(24, 13);
            this.label80.TabIndex = 36;
            this.label80.Text = "Min";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(120, 28);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(27, 13);
            this.label81.TabIndex = 35;
            this.label81.Text = "Max";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(7, 217);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(64, 13);
            this.label82.TabIndex = 34;
            this.label82.Text = "BP Diastolic";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(7, 175);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(60, 13);
            this.label83.TabIndex = 33;
            this.label83.Text = "BP Systolic";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(3, 130);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(67, 13);
            this.label84.TabIndex = 32;
            this.label84.Text = "Temperature";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(15, 80);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(33, 13);
            this.label85.TabIndex = 31;
            this.label85.Text = "Pulse";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(7, 40);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(52, 13);
            this.label86.TabIndex = 30;
            this.label86.Text = "Breathing";
            // 
            // bpd6Max
            // 
            this.bpd6Max.Location = new System.Drawing.Point(123, 215);
            this.bpd6Max.Name = "bpd6Max";
            this.bpd6Max.Size = new System.Drawing.Size(45, 20);
            this.bpd6Max.TabIndex = 24;
            this.bpd6Max.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // bpd6Min
            // 
            this.bpd6Min.Location = new System.Drawing.Point(72, 215);
            this.bpd6Min.Name = "bpd6Min";
            this.bpd6Min.Size = new System.Drawing.Size(45, 20);
            this.bpd6Min.TabIndex = 23;
            this.bpd6Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // bps6Max
            // 
            this.bps6Max.Location = new System.Drawing.Point(123, 173);
            this.bps6Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.bps6Max.Name = "bps6Max";
            this.bps6Max.Size = new System.Drawing.Size(45, 20);
            this.bps6Max.TabIndex = 22;
            this.bps6Max.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // bps6Min
            // 
            this.bps6Min.Location = new System.Drawing.Point(72, 173);
            this.bps6Min.Name = "bps6Min";
            this.bps6Min.Size = new System.Drawing.Size(45, 20);
            this.bps6Min.TabIndex = 21;
            this.bps6Min.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // te6Max
            // 
            this.te6Max.DecimalPlaces = 1;
            this.te6Max.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te6Max.Location = new System.Drawing.Point(123, 128);
            this.te6Max.Name = "te6Max";
            this.te6Max.Size = new System.Drawing.Size(45, 20);
            this.te6Max.TabIndex = 20;
            this.te6Max.Value = new decimal(new int[] {
            372,
            0,
            0,
            65536});
            // 
            // te6Min
            // 
            this.te6Min.DecimalPlaces = 1;
            this.te6Min.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te6Min.Location = new System.Drawing.Point(72, 128);
            this.te6Min.Name = "te6Min";
            this.te6Min.Size = new System.Drawing.Size(45, 20);
            this.te6Min.TabIndex = 19;
            this.te6Min.Value = new decimal(new int[] {
            365,
            0,
            0,
            65536});
            // 
            // pr6Max
            // 
            this.pr6Max.Location = new System.Drawing.Point(123, 85);
            this.pr6Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.pr6Max.Name = "pr6Max";
            this.pr6Max.Size = new System.Drawing.Size(45, 20);
            this.pr6Max.TabIndex = 18;
            this.pr6Max.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pr6Min
            // 
            this.pr6Min.Location = new System.Drawing.Point(72, 85);
            this.pr6Min.Name = "pr6Min";
            this.pr6Min.Size = new System.Drawing.Size(45, 20);
            this.pr6Min.TabIndex = 17;
            this.pr6Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // br6Max
            // 
            this.br6Max.Location = new System.Drawing.Point(123, 44);
            this.br6Max.Name = "br6Max";
            this.br6Max.Size = new System.Drawing.Size(45, 20);
            this.br6Max.TabIndex = 16;
            this.br6Max.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // br6Min
            // 
            this.br6Min.Location = new System.Drawing.Point(72, 44);
            this.br6Min.Name = "br6Min";
            this.br6Min.Size = new System.Drawing.Size(45, 20);
            this.br6Min.TabIndex = 15;
            this.br6Min.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // p7
            // 
            this.p7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("p7.BackgroundImage")));
            this.p7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.p7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.p7.Controls.Add(this.l7re);
            this.p7.Controls.Add(this.bp7s);
            this.p7.Controls.Add(this.label87);
            this.p7.Controls.Add(this.bp7d);
            this.p7.Controls.Add(this.label88);
            this.p7.Controls.Add(this.te7s);
            this.p7.Controls.Add(this.label89);
            this.p7.Controls.Add(this.te7d);
            this.p7.Controls.Add(this.l7Bpd);
            this.p7.Controls.Add(this.pr7s);
            this.p7.Controls.Add(this.l7Bps);
            this.p7.Controls.Add(this.pr7d);
            this.p7.Controls.Add(this.l7Te);
            this.p7.Controls.Add(this.br7s);
            this.p7.Controls.Add(this.br7d);
            this.p7.Controls.Add(this.l7Pr);
            this.p7.Controls.Add(this.l7Br);
            this.p7.Controls.Add(this.label95);
            this.p7.Controls.Add(this.m7);
            this.p7.Controls.Add(this.label96);
            this.p7.Controls.Add(this.label97);
            this.p7.Controls.Add(this.label98);
            this.p7.Controls.Add(this.label99);
            this.p7.Controls.Add(this.label100);
            this.p7.Controls.Add(this.label101);
            this.p7.Controls.Add(this.label102);
            this.p7.Controls.Add(this.bpd7Max);
            this.p7.Controls.Add(this.bpd7Min);
            this.p7.Controls.Add(this.bps7Max);
            this.p7.Controls.Add(this.bps7Min);
            this.p7.Controls.Add(this.te7Max);
            this.p7.Controls.Add(this.te7Min);
            this.p7.Controls.Add(this.pr7Max);
            this.p7.Controls.Add(this.pr7Min);
            this.p7.Controls.Add(this.br7Max);
            this.p7.Controls.Add(this.br7Min);
            this.p7.Location = new System.Drawing.Point(628, 314);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(259, 240);
            this.p7.TabIndex = 50;
            // 
            // l7re
            // 
            this.l7re.Location = new System.Drawing.Point(205, 208);
            this.l7re.Name = "l7re";
            this.l7re.Size = new System.Drawing.Size(44, 22);
            this.l7re.TabIndex = 54;
            this.l7re.Text = "Reset";
            this.l7re.UseVisualStyleBackColor = true;
            this.l7re.Click += new System.EventHandler(this.l7Re_Click);
            // 
            // bp7s
            // 
            this.bp7s.AutoSize = true;
            this.bp7s.Location = new System.Drawing.Point(216, 176);
            this.bp7s.Name = "bp7s";
            this.bp7s.Size = new System.Drawing.Size(34, 13);
            this.bp7s.TabIndex = 100;
            this.bp7s.Text = "Selec";
            this.bp7s.Click += new System.EventHandler(this.bp7s_Click);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(16, 93);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(30, 13);
            this.label87.TabIndex = 47;
            this.label87.Text = "Rate";
            // 
            // bp7d
            // 
            this.bp7d.AutoSize = true;
            this.bp7d.Location = new System.Drawing.Point(202, 176);
            this.bp7d.Name = "bp7d";
            this.bp7d.Size = new System.Drawing.Size(54, 13);
            this.bp7d.TabIndex = 99;
            this.bp7d.Text = "De Select";
            this.bp7d.Click += new System.EventHandler(this.bp7d_Click);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(19, 53);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(30, 13);
            this.label88.TabIndex = 46;
            this.label88.Text = "Rate";
            // 
            // te7s
            // 
            this.te7s.AutoSize = true;
            this.te7s.Location = new System.Drawing.Point(216, 129);
            this.te7s.Name = "te7s";
            this.te7s.Size = new System.Drawing.Size(34, 13);
            this.te7s.TabIndex = 98;
            this.te7s.Text = "Selec";
            this.te7s.Click += new System.EventHandler(this.te7s_Click);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(172, 28);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(41, 13);
            this.label89.TabIndex = 45;
            this.label89.Text = "Current";
            // 
            // te7d
            // 
            this.te7d.AutoSize = true;
            this.te7d.Location = new System.Drawing.Point(202, 129);
            this.te7d.Name = "te7d";
            this.te7d.Size = new System.Drawing.Size(54, 13);
            this.te7d.TabIndex = 97;
            this.te7d.Text = "De Select";
            this.te7d.Click += new System.EventHandler(this.te7d_Click);
            // 
            // l7Bpd
            // 
            this.l7Bpd.AutoSize = true;
            this.l7Bpd.Location = new System.Drawing.Point(175, 217);
            this.l7Bpd.Name = "l7Bpd";
            this.l7Bpd.Size = new System.Drawing.Size(19, 13);
            this.l7Bpd.TabIndex = 44;
            this.l7Bpd.Text = "70";
            // 
            // pr7s
            // 
            this.pr7s.AutoSize = true;
            this.pr7s.Location = new System.Drawing.Point(216, 87);
            this.pr7s.Name = "pr7s";
            this.pr7s.Size = new System.Drawing.Size(34, 13);
            this.pr7s.TabIndex = 96;
            this.pr7s.Text = "Selec";
            this.pr7s.Click += new System.EventHandler(this.pr7s_Click);
            // 
            // l7Bps
            // 
            this.l7Bps.AutoSize = true;
            this.l7Bps.Location = new System.Drawing.Point(175, 175);
            this.l7Bps.Name = "l7Bps";
            this.l7Bps.Size = new System.Drawing.Size(25, 13);
            this.l7Bps.TabIndex = 43;
            this.l7Bps.Text = "105";
            // 
            // pr7d
            // 
            this.pr7d.AutoSize = true;
            this.pr7d.Location = new System.Drawing.Point(202, 87);
            this.pr7d.Name = "pr7d";
            this.pr7d.Size = new System.Drawing.Size(54, 13);
            this.pr7d.TabIndex = 95;
            this.pr7d.Text = "De Select";
            this.pr7d.Click += new System.EventHandler(this.pr7d_Click);
            // 
            // l7Te
            // 
            this.l7Te.AutoSize = true;
            this.l7Te.Location = new System.Drawing.Point(175, 130);
            this.l7Te.Name = "l7Te";
            this.l7Te.Size = new System.Drawing.Size(28, 13);
            this.l7Te.TabIndex = 42;
            this.l7Te.Text = "36.9";
            // 
            // br7s
            // 
            this.br7s.AutoSize = true;
            this.br7s.Location = new System.Drawing.Point(216, 46);
            this.br7s.Name = "br7s";
            this.br7s.Size = new System.Drawing.Size(34, 13);
            this.br7s.TabIndex = 94;
            this.br7s.Text = "Selec";
            this.br7s.Click += new System.EventHandler(this.br7s_Click);
            // 
            // br7d
            // 
            this.br7d.AutoSize = true;
            this.br7d.Location = new System.Drawing.Point(202, 46);
            this.br7d.Name = "br7d";
            this.br7d.Size = new System.Drawing.Size(54, 13);
            this.br7d.TabIndex = 93;
            this.br7d.Text = "De Select";
            this.br7d.Click += new System.EventHandler(this.br7d_Click);
            // 
            // l7Pr
            // 
            this.l7Pr.AutoSize = true;
            this.l7Pr.Location = new System.Drawing.Point(175, 87);
            this.l7Pr.Name = "l7Pr";
            this.l7Pr.Size = new System.Drawing.Size(19, 13);
            this.l7Pr.TabIndex = 41;
            this.l7Pr.Text = "80";
            // 
            // l7Br
            // 
            this.l7Br.AutoSize = true;
            this.l7Br.Location = new System.Drawing.Point(175, 46);
            this.l7Br.Name = "l7Br";
            this.l7Br.Size = new System.Drawing.Size(19, 13);
            this.l7Br.TabIndex = 40;
            this.l7Br.Text = "16";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(221, 4);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(35, 13);
            this.label95.TabIndex = 39;
            this.label95.Text = "Bed 7";
            // 
            // m7
            // 
            this.m7.AutoSize = true;
            this.m7.Location = new System.Drawing.Point(10, 8);
            this.m7.Name = "m7";
            this.m7.Size = new System.Drawing.Size(50, 17);
            this.m7.TabIndex = 38;
            this.m7.Text = "Mute";
            this.m7.UseVisualStyleBackColor = true;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(70, 28);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(24, 13);
            this.label96.TabIndex = 36;
            this.label96.Text = "Min";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(121, 28);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(27, 13);
            this.label97.TabIndex = 35;
            this.label97.Text = "Max";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(8, 217);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(64, 13);
            this.label98.TabIndex = 34;
            this.label98.Text = "BP Diastolic";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(8, 175);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(60, 13);
            this.label99.TabIndex = 33;
            this.label99.Text = "BP Systolic";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(4, 130);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(67, 13);
            this.label100.TabIndex = 32;
            this.label100.Text = "Temperature";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(16, 80);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(33, 13);
            this.label101.TabIndex = 31;
            this.label101.Text = "Pulse";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(8, 40);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(52, 13);
            this.label102.TabIndex = 30;
            this.label102.Text = "Breathing";
            // 
            // bpd7Max
            // 
            this.bpd7Max.Location = new System.Drawing.Point(124, 215);
            this.bpd7Max.Name = "bpd7Max";
            this.bpd7Max.Size = new System.Drawing.Size(45, 20);
            this.bpd7Max.TabIndex = 24;
            this.bpd7Max.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // bpd7Min
            // 
            this.bpd7Min.Location = new System.Drawing.Point(73, 215);
            this.bpd7Min.Name = "bpd7Min";
            this.bpd7Min.Size = new System.Drawing.Size(45, 20);
            this.bpd7Min.TabIndex = 23;
            this.bpd7Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // bps7Max
            // 
            this.bps7Max.Location = new System.Drawing.Point(124, 173);
            this.bps7Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.bps7Max.Name = "bps7Max";
            this.bps7Max.Size = new System.Drawing.Size(45, 20);
            this.bps7Max.TabIndex = 22;
            this.bps7Max.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // bps7Min
            // 
            this.bps7Min.Location = new System.Drawing.Point(73, 173);
            this.bps7Min.Name = "bps7Min";
            this.bps7Min.Size = new System.Drawing.Size(45, 20);
            this.bps7Min.TabIndex = 21;
            this.bps7Min.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // te7Max
            // 
            this.te7Max.DecimalPlaces = 1;
            this.te7Max.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te7Max.Location = new System.Drawing.Point(124, 128);
            this.te7Max.Name = "te7Max";
            this.te7Max.Size = new System.Drawing.Size(45, 20);
            this.te7Max.TabIndex = 20;
            this.te7Max.Value = new decimal(new int[] {
            372,
            0,
            0,
            65536});
            // 
            // te7Min
            // 
            this.te7Min.DecimalPlaces = 1;
            this.te7Min.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te7Min.Location = new System.Drawing.Point(73, 128);
            this.te7Min.Name = "te7Min";
            this.te7Min.Size = new System.Drawing.Size(45, 20);
            this.te7Min.TabIndex = 19;
            this.te7Min.Value = new decimal(new int[] {
            365,
            0,
            0,
            65536});
            // 
            // pr7Max
            // 
            this.pr7Max.Location = new System.Drawing.Point(124, 85);
            this.pr7Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.pr7Max.Name = "pr7Max";
            this.pr7Max.Size = new System.Drawing.Size(45, 20);
            this.pr7Max.TabIndex = 18;
            this.pr7Max.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pr7Min
            // 
            this.pr7Min.Location = new System.Drawing.Point(73, 85);
            this.pr7Min.Name = "pr7Min";
            this.pr7Min.Size = new System.Drawing.Size(45, 20);
            this.pr7Min.TabIndex = 17;
            this.pr7Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // br7Max
            // 
            this.br7Max.Location = new System.Drawing.Point(124, 44);
            this.br7Max.Name = "br7Max";
            this.br7Max.Size = new System.Drawing.Size(45, 20);
            this.br7Max.TabIndex = 16;
            this.br7Max.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // br7Min
            // 
            this.br7Min.Location = new System.Drawing.Point(73, 44);
            this.br7Min.Name = "br7Min";
            this.br7Min.Size = new System.Drawing.Size(45, 20);
            this.br7Min.TabIndex = 15;
            this.br7Min.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // p8
            // 
            this.p8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("p8.BackgroundImage")));
            this.p8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.p8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.p8.Controls.Add(this.l8re);
            this.p8.Controls.Add(this.bp8s);
            this.p8.Controls.Add(this.label103);
            this.p8.Controls.Add(this.bp8d);
            this.p8.Controls.Add(this.label104);
            this.p8.Controls.Add(this.te8s);
            this.p8.Controls.Add(this.label105);
            this.p8.Controls.Add(this.te8d);
            this.p8.Controls.Add(this.l8Bpd);
            this.p8.Controls.Add(this.pr8s);
            this.p8.Controls.Add(this.l8Bps);
            this.p8.Controls.Add(this.pr8d);
            this.p8.Controls.Add(this.l8Te);
            this.p8.Controls.Add(this.br8s);
            this.p8.Controls.Add(this.br8d);
            this.p8.Controls.Add(this.l8Pr);
            this.p8.Controls.Add(this.l8Br);
            this.p8.Controls.Add(this.label111);
            this.p8.Controls.Add(this.m8);
            this.p8.Controls.Add(this.label112);
            this.p8.Controls.Add(this.label113);
            this.p8.Controls.Add(this.label114);
            this.p8.Controls.Add(this.label115);
            this.p8.Controls.Add(this.label116);
            this.p8.Controls.Add(this.label117);
            this.p8.Controls.Add(this.label118);
            this.p8.Controls.Add(this.bpd8Max);
            this.p8.Controls.Add(this.bpd8Min);
            this.p8.Controls.Add(this.bps8Max);
            this.p8.Controls.Add(this.bps8Min);
            this.p8.Controls.Add(this.te8Max);
            this.p8.Controls.Add(this.te8Min);
            this.p8.Controls.Add(this.pr8Max);
            this.p8.Controls.Add(this.pr8Min);
            this.p8.Controls.Add(this.br8Max);
            this.p8.Controls.Add(this.br8Min);
            this.p8.Location = new System.Drawing.Point(953, 314);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(259, 240);
            this.p8.TabIndex = 50;
            // 
            // l8re
            // 
            this.l8re.Location = new System.Drawing.Point(207, 212);
            this.l8re.Name = "l8re";
            this.l8re.Size = new System.Drawing.Size(44, 22);
            this.l8re.TabIndex = 55;
            this.l8re.Text = "Reset";
            this.l8re.UseVisualStyleBackColor = true;
            this.l8re.Click += new System.EventHandler(this.l8Re_Click);
            // 
            // bp8s
            // 
            this.bp8s.AutoSize = true;
            this.bp8s.Location = new System.Drawing.Point(218, 176);
            this.bp8s.Name = "bp8s";
            this.bp8s.Size = new System.Drawing.Size(34, 13);
            this.bp8s.TabIndex = 110;
            this.bp8s.Text = "Selec";
            this.bp8s.Click += new System.EventHandler(this.bp8s_Click);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(16, 93);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(30, 13);
            this.label103.TabIndex = 47;
            this.label103.Text = "Rate";
            // 
            // bp8d
            // 
            this.bp8d.AutoSize = true;
            this.bp8d.Location = new System.Drawing.Point(204, 176);
            this.bp8d.Name = "bp8d";
            this.bp8d.Size = new System.Drawing.Size(54, 13);
            this.bp8d.TabIndex = 109;
            this.bp8d.Text = "De Select";
            this.bp8d.Click += new System.EventHandler(this.bp8d_Click);
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(19, 53);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(30, 13);
            this.label104.TabIndex = 46;
            this.label104.Text = "Rate";
            // 
            // te8s
            // 
            this.te8s.AutoSize = true;
            this.te8s.Location = new System.Drawing.Point(218, 129);
            this.te8s.Name = "te8s";
            this.te8s.Size = new System.Drawing.Size(34, 13);
            this.te8s.TabIndex = 108;
            this.te8s.Text = "Selec";
            this.te8s.Click += new System.EventHandler(this.te8s_Click);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(172, 28);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(41, 13);
            this.label105.TabIndex = 45;
            this.label105.Text = "Current";
            // 
            // te8d
            // 
            this.te8d.AutoSize = true;
            this.te8d.Location = new System.Drawing.Point(204, 129);
            this.te8d.Name = "te8d";
            this.te8d.Size = new System.Drawing.Size(54, 13);
            this.te8d.TabIndex = 107;
            this.te8d.Text = "De Select";
            this.te8d.Click += new System.EventHandler(this.te8d_Click);
            // 
            // l8Bpd
            // 
            this.l8Bpd.AutoSize = true;
            this.l8Bpd.Location = new System.Drawing.Point(175, 217);
            this.l8Bpd.Name = "l8Bpd";
            this.l8Bpd.Size = new System.Drawing.Size(19, 13);
            this.l8Bpd.TabIndex = 44;
            this.l8Bpd.Text = "70";
            // 
            // pr8s
            // 
            this.pr8s.AutoSize = true;
            this.pr8s.Location = new System.Drawing.Point(218, 87);
            this.pr8s.Name = "pr8s";
            this.pr8s.Size = new System.Drawing.Size(34, 13);
            this.pr8s.TabIndex = 106;
            this.pr8s.Text = "Selec";
            this.pr8s.Click += new System.EventHandler(this.pr8s_Click);
            // 
            // l8Bps
            // 
            this.l8Bps.AutoSize = true;
            this.l8Bps.Location = new System.Drawing.Point(175, 175);
            this.l8Bps.Name = "l8Bps";
            this.l8Bps.Size = new System.Drawing.Size(25, 13);
            this.l8Bps.TabIndex = 43;
            this.l8Bps.Text = "105";
            // 
            // pr8d
            // 
            this.pr8d.AutoSize = true;
            this.pr8d.Location = new System.Drawing.Point(204, 87);
            this.pr8d.Name = "pr8d";
            this.pr8d.Size = new System.Drawing.Size(54, 13);
            this.pr8d.TabIndex = 105;
            this.pr8d.Text = "De Select";
            this.pr8d.Click += new System.EventHandler(this.pr8d_Click);
            // 
            // l8Te
            // 
            this.l8Te.AutoSize = true;
            this.l8Te.Location = new System.Drawing.Point(175, 130);
            this.l8Te.Name = "l8Te";
            this.l8Te.Size = new System.Drawing.Size(28, 13);
            this.l8Te.TabIndex = 42;
            this.l8Te.Text = "36.9";
            // 
            // br8s
            // 
            this.br8s.AutoSize = true;
            this.br8s.Location = new System.Drawing.Point(218, 46);
            this.br8s.Name = "br8s";
            this.br8s.Size = new System.Drawing.Size(34, 13);
            this.br8s.TabIndex = 104;
            this.br8s.Text = "Selec";
            this.br8s.Click += new System.EventHandler(this.br8s_Click);
            // 
            // br8d
            // 
            this.br8d.AutoSize = true;
            this.br8d.Location = new System.Drawing.Point(204, 46);
            this.br8d.Name = "br8d";
            this.br8d.Size = new System.Drawing.Size(54, 13);
            this.br8d.TabIndex = 103;
            this.br8d.Text = "De Select";
            this.br8d.Click += new System.EventHandler(this.br8d_Click);
            // 
            // l8Pr
            // 
            this.l8Pr.AutoSize = true;
            this.l8Pr.Location = new System.Drawing.Point(175, 87);
            this.l8Pr.Name = "l8Pr";
            this.l8Pr.Size = new System.Drawing.Size(19, 13);
            this.l8Pr.TabIndex = 41;
            this.l8Pr.Text = "80";
            // 
            // l8Br
            // 
            this.l8Br.AutoSize = true;
            this.l8Br.Location = new System.Drawing.Point(175, 46);
            this.l8Br.Name = "l8Br";
            this.l8Br.Size = new System.Drawing.Size(19, 13);
            this.l8Br.TabIndex = 40;
            this.l8Br.Text = "16";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(220, 8);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(35, 13);
            this.label111.TabIndex = 39;
            this.label111.Text = "Bed 8";
            // 
            // m8
            // 
            this.m8.AutoSize = true;
            this.m8.Location = new System.Drawing.Point(10, 8);
            this.m8.Name = "m8";
            this.m8.Size = new System.Drawing.Size(50, 17);
            this.m8.TabIndex = 38;
            this.m8.Text = "Mute";
            this.m8.UseVisualStyleBackColor = true;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(70, 28);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(24, 13);
            this.label112.TabIndex = 36;
            this.label112.Text = "Min";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(121, 28);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(27, 13);
            this.label113.TabIndex = 35;
            this.label113.Text = "Max";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(8, 217);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(64, 13);
            this.label114.TabIndex = 34;
            this.label114.Text = "BP Diastolic";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(8, 175);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(60, 13);
            this.label115.TabIndex = 33;
            this.label115.Text = "BP Systolic";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(4, 130);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(67, 13);
            this.label116.TabIndex = 32;
            this.label116.Text = "Temperature";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(16, 80);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(33, 13);
            this.label117.TabIndex = 31;
            this.label117.Text = "Pulse";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(8, 40);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(52, 13);
            this.label118.TabIndex = 30;
            this.label118.Text = "Breathing";
            // 
            // bpd8Max
            // 
            this.bpd8Max.Location = new System.Drawing.Point(124, 215);
            this.bpd8Max.Name = "bpd8Max";
            this.bpd8Max.Size = new System.Drawing.Size(45, 20);
            this.bpd8Max.TabIndex = 24;
            this.bpd8Max.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // bpd8Min
            // 
            this.bpd8Min.Location = new System.Drawing.Point(73, 215);
            this.bpd8Min.Name = "bpd8Min";
            this.bpd8Min.Size = new System.Drawing.Size(45, 20);
            this.bpd8Min.TabIndex = 23;
            this.bpd8Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // bps8Max
            // 
            this.bps8Max.Location = new System.Drawing.Point(124, 173);
            this.bps8Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.bps8Max.Name = "bps8Max";
            this.bps8Max.Size = new System.Drawing.Size(45, 20);
            this.bps8Max.TabIndex = 22;
            this.bps8Max.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // bps8Min
            // 
            this.bps8Min.Location = new System.Drawing.Point(73, 173);
            this.bps8Min.Name = "bps8Min";
            this.bps8Min.Size = new System.Drawing.Size(45, 20);
            this.bps8Min.TabIndex = 21;
            this.bps8Min.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // te8Max
            // 
            this.te8Max.DecimalPlaces = 1;
            this.te8Max.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te8Max.Location = new System.Drawing.Point(124, 128);
            this.te8Max.Name = "te8Max";
            this.te8Max.Size = new System.Drawing.Size(45, 20);
            this.te8Max.TabIndex = 20;
            this.te8Max.Value = new decimal(new int[] {
            372,
            0,
            0,
            65536});
            // 
            // te8Min
            // 
            this.te8Min.DecimalPlaces = 1;
            this.te8Min.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.te8Min.Location = new System.Drawing.Point(73, 128);
            this.te8Min.Name = "te8Min";
            this.te8Min.Size = new System.Drawing.Size(45, 20);
            this.te8Min.TabIndex = 19;
            this.te8Min.Value = new decimal(new int[] {
            365,
            0,
            0,
            65536});
            // 
            // pr8Max
            // 
            this.pr8Max.Location = new System.Drawing.Point(124, 85);
            this.pr8Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.pr8Max.Name = "pr8Max";
            this.pr8Max.Size = new System.Drawing.Size(45, 20);
            this.pr8Max.TabIndex = 18;
            this.pr8Max.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pr8Min
            // 
            this.pr8Min.Location = new System.Drawing.Point(73, 85);
            this.pr8Min.Name = "pr8Min";
            this.pr8Min.Size = new System.Drawing.Size(45, 20);
            this.pr8Min.TabIndex = 17;
            this.pr8Min.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // br8Max
            // 
            this.br8Max.Location = new System.Drawing.Point(124, 44);
            this.br8Max.Name = "br8Max";
            this.br8Max.Size = new System.Drawing.Size(45, 20);
            this.br8Max.TabIndex = 16;
            this.br8Max.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // br8Min
            // 
            this.br8Min.Location = new System.Drawing.Point(73, 44);
            this.br8Min.Name = "br8Min";
            this.br8Min.Size = new System.Drawing.Size(45, 20);
            this.br8Min.TabIndex = 15;
            this.br8Min.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // a1
            // 
            this.a1.Location = new System.Drawing.Point(104, 12);
            this.a1.Name = "a1";
            this.a1.Size = new System.Drawing.Size(56, 21);
            this.a1.TabIndex = 50;
            this.a1.Text = "Activate";
            this.a1.UseVisualStyleBackColor = true;
            this.a1.Click += new System.EventHandler(this.button7_Click);
            // 
            // a3
            // 
            this.a3.Location = new System.Drawing.Point(721, 12);
            this.a3.Name = "a3";
            this.a3.Size = new System.Drawing.Size(64, 21);
            this.a3.TabIndex = 52;
            this.a3.Text = "Activate";
            this.a3.UseVisualStyleBackColor = true;
            this.a3.Click += new System.EventHandler(this.a3_Click);
            // 
            // a4
            // 
            this.a4.Location = new System.Drawing.Point(1032, 12);
            this.a4.Name = "a4";
            this.a4.Size = new System.Drawing.Size(64, 21);
            this.a4.TabIndex = 56;
            this.a4.Text = "Activate";
            this.a4.UseVisualStyleBackColor = true;
            this.a4.Click += new System.EventHandler(this.a4_Click);
            // 
            // a5
            // 
            this.a5.Location = new System.Drawing.Point(104, 291);
            this.a5.Name = "a5";
            this.a5.Size = new System.Drawing.Size(56, 21);
            this.a5.TabIndex = 57;
            this.a5.Text = "Activate";
            this.a5.UseVisualStyleBackColor = true;
            this.a5.Click += new System.EventHandler(this.a5_Click);
            // 
            // a6
            // 
            this.a6.Location = new System.Drawing.Point(412, 291);
            this.a6.Name = "a6";
            this.a6.Size = new System.Drawing.Size(64, 21);
            this.a6.TabIndex = 58;
            this.a6.Text = "Activate";
            this.a6.UseVisualStyleBackColor = true;
            this.a6.Click += new System.EventHandler(this.a6_Click);
            // 
            // a7
            // 
            this.a7.Location = new System.Drawing.Point(721, 287);
            this.a7.Name = "a7";
            this.a7.Size = new System.Drawing.Size(64, 21);
            this.a7.TabIndex = 59;
            this.a7.Text = "Activate";
            this.a7.UseVisualStyleBackColor = true;
            this.a7.Click += new System.EventHandler(this.a7_Click);
            // 
            // a8
            // 
            this.a8.Location = new System.Drawing.Point(1032, 293);
            this.a8.Name = "a8";
            this.a8.Size = new System.Drawing.Size(64, 21);
            this.a8.TabIndex = 60;
            this.a8.Text = "Activate";
            this.a8.UseVisualStyleBackColor = true;
            this.a8.Click += new System.EventHandler(this.a8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(1227, 32);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(64, 21);
            this.button7.TabIndex = 61;
            this.button7.Text = "Logout";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // shiftBindingSource
            // 
            this.shiftBindingSource.DataMember = "Shift";
            this.shiftBindingSource.DataSource = this.bedsideMonitorDataSet1;
            this.shiftBindingSource.CurrentChanged += new System.EventHandler(this.shiftBindingSource_CurrentChanged);
            // 
            // bedsideMonitorDataSet1
            // 
            this.bedsideMonitorDataSet1.DataSetName = "BedsideMonitorDataSet1";
            this.bedsideMonitorDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // shiftTableAdapter
            // 
            this.shiftTableAdapter.ClearBeforeFill = true;
            // 
            // a2
            // 
            this.a2.Location = new System.Drawing.Point(412, 12);
            this.a2.Name = "a2";
            this.a2.Size = new System.Drawing.Size(64, 21);
            this.a2.TabIndex = 63;
            this.a2.Text = "Activate";
            this.a2.UseVisualStyleBackColor = true;
            this.a2.Click += new System.EventHandler(this.a2_Click);
            // 
            // frmCentralStation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(1313, 558);
            this.Controls.Add(this.a2);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.a8);
            this.Controls.Add(this.a7);
            this.Controls.Add(this.a6);
            this.Controls.Add(this.a5);
            this.Controls.Add(this.a4);
            this.Controls.Add(this.a3);
            this.Controls.Add(this.a1);
            this.Controls.Add(this.p8);
            this.Controls.Add(this.p7);
            this.Controls.Add(this.p6);
            this.Controls.Add(this.p5);
            this.Controls.Add(this.p4);
            this.Controls.Add(this.p3);
            this.Controls.Add(this.p2);
            this.Controls.Add(this.p1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCentralStation";
            this.Text = "Central Station";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.p1.ResumeLayout(false);
            this.p1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te1max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br2Min)).EndInit();
            this.p2.ResumeLayout(false);
            this.p2.PerformLayout();
            this.p3.ResumeLayout(false);
            this.p3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br3Min)).EndInit();
            this.p4.ResumeLayout(false);
            this.p4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br4Min)).EndInit();
            this.p5.ResumeLayout(false);
            this.p5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd5Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd5Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps5Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps5Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te5Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te5Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr5Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr5Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br5Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br5Min)).EndInit();
            this.p6.ResumeLayout(false);
            this.p6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd6Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd6Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps6Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps6Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te6Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te6Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr6Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr6Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br6Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br6Min)).EndInit();
            this.p7.ResumeLayout(false);
            this.p7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd7Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd7Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps7Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps7Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te7Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te7Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr7Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr7Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br7Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br7Min)).EndInit();
            this.p8.ResumeLayout(false);
            this.p8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bpd8Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bpd8Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps8Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bps8Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te8Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.te8Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr8Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pr8Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br8Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.br8Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shiftBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bedsideMonitorDataSet1)).EndInit();
            this.ResumeLayout(false);

        }

        public void panel1_Paint(object sender, PaintEventArgs e)
        {
          
        }

        #endregion

        private System.Windows.Forms.Panel p1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label l1Bpd;
        private System.Windows.Forms.Label l1Bps;
        private System.Windows.Forms.Label l1Te;
        private System.Windows.Forms.Label l1Pr;
        private System.Windows.Forms.Label l1Br;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox m1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown bpd1Max;
        private System.Windows.Forms.NumericUpDown bpd1Min;
        private System.Windows.Forms.NumericUpDown bps1Max;
        private System.Windows.Forms.NumericUpDown bps1Min;
        private System.Windows.Forms.NumericUpDown te1max;
        private System.Windows.Forms.NumericUpDown te1Min;
        private System.Windows.Forms.NumericUpDown pr1Max;
        private System.Windows.Forms.NumericUpDown pr1Min;
        private System.Windows.Forms.NumericUpDown br1Max;
        private System.Windows.Forms.NumericUpDown br1Min;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label l2Bpd;
        private System.Windows.Forms.Label l2Bps;
        private System.Windows.Forms.Label l2Te;
        private System.Windows.Forms.Label l2Pr;
        private System.Windows.Forms.Label l2Br;
        private System.Windows.Forms.Label m2;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown bpd2Max;
        private System.Windows.Forms.NumericUpDown bpd2Min;
        private System.Windows.Forms.NumericUpDown bps2Max;
        private System.Windows.Forms.NumericUpDown bps2Min;
        private System.Windows.Forms.NumericUpDown te2Max;
        private System.Windows.Forms.NumericUpDown te2Min;
        private System.Windows.Forms.NumericUpDown pr2Max;
        private System.Windows.Forms.NumericUpDown pr2Min;
        private System.Windows.Forms.NumericUpDown br2Max;
        private System.Windows.Forms.NumericUpDown br2Min;
        private System.Windows.Forms.Panel p2;
        private System.Windows.Forms.Panel p3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label l3Bpd;
        private System.Windows.Forms.Label l3Bps;
        private System.Windows.Forms.Label l3Te;
        private System.Windows.Forms.Label l3Pr;
        private System.Windows.Forms.Label l3Br;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.CheckBox m3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.NumericUpDown bpd3Max;
        private System.Windows.Forms.NumericUpDown bpd3Min;
        private System.Windows.Forms.NumericUpDown bps3Max;
        private System.Windows.Forms.NumericUpDown bps3Min;
        private System.Windows.Forms.NumericUpDown te3Max;
        private System.Windows.Forms.NumericUpDown te3Min;
        private System.Windows.Forms.NumericUpDown pr3Max;
        private System.Windows.Forms.NumericUpDown pr3Min;
        private System.Windows.Forms.NumericUpDown br3Max;
        private System.Windows.Forms.NumericUpDown br3Min;
        private System.Windows.Forms.Panel p4;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label l4Bpd;
        private System.Windows.Forms.Label l4Bps;
        private System.Windows.Forms.Label l4Te;
        private System.Windows.Forms.Label l4Pr;
        private System.Windows.Forms.Label l4Br;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.CheckBox m4;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.NumericUpDown bpd4Max;
        private System.Windows.Forms.NumericUpDown bpd4Min;
        private System.Windows.Forms.NumericUpDown bps4Max;
        private System.Windows.Forms.NumericUpDown bps4Min;
        private System.Windows.Forms.NumericUpDown te4Max;
        private System.Windows.Forms.NumericUpDown te4Min;
        private System.Windows.Forms.NumericUpDown pr4Max;
        private System.Windows.Forms.NumericUpDown pr4Min;
        private System.Windows.Forms.NumericUpDown br4Max;
        private System.Windows.Forms.NumericUpDown br4Min;
        private System.Windows.Forms.Panel p5;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label l5Bpd;
        private System.Windows.Forms.Label l5Bps;
        private System.Windows.Forms.Label l5Te;
        private System.Windows.Forms.Label l5Pr;
        private System.Windows.Forms.Label l5Br;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.CheckBox m5;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.NumericUpDown bpd5Max;
        private System.Windows.Forms.NumericUpDown bpd5Min;
        private System.Windows.Forms.NumericUpDown bps5Max;
        private System.Windows.Forms.NumericUpDown bps5Min;
        private System.Windows.Forms.NumericUpDown te5Max;
        private System.Windows.Forms.NumericUpDown te5Min;
        private System.Windows.Forms.NumericUpDown pr5Max;
        private System.Windows.Forms.NumericUpDown pr5Min;
        private System.Windows.Forms.NumericUpDown br5Max;
        private System.Windows.Forms.NumericUpDown br5Min;
        private System.Windows.Forms.Panel p6;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label l6Bpd;
        private System.Windows.Forms.Label l6Bps;
        private System.Windows.Forms.Label l6Te;
        private System.Windows.Forms.Label l6Pr;
        private System.Windows.Forms.Label l6Br;
        private System.Windows.Forms.Label m6;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.NumericUpDown bpd6Max;
        private System.Windows.Forms.NumericUpDown bpd6Min;
        private System.Windows.Forms.NumericUpDown bps6Max;
        private System.Windows.Forms.NumericUpDown bps6Min;
        private System.Windows.Forms.NumericUpDown te6Max;
        private System.Windows.Forms.NumericUpDown te6Min;
        private System.Windows.Forms.NumericUpDown pr6Max;
        private System.Windows.Forms.NumericUpDown pr6Min;
        private System.Windows.Forms.NumericUpDown br6Max;
        private System.Windows.Forms.NumericUpDown br6Min;
        private System.Windows.Forms.Panel p7;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label l7Bpd;
        private System.Windows.Forms.Label l7Bps;
        private System.Windows.Forms.Label l7Te;
        private System.Windows.Forms.Label l7Pr;
        private System.Windows.Forms.Label l7Br;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.CheckBox m7;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.NumericUpDown bpd7Max;
        private System.Windows.Forms.NumericUpDown bpd7Min;
        private System.Windows.Forms.NumericUpDown bps7Max;
        private System.Windows.Forms.NumericUpDown bps7Min;
        private System.Windows.Forms.NumericUpDown te7Max;
        private System.Windows.Forms.NumericUpDown te7Min;
        private System.Windows.Forms.NumericUpDown pr7Max;
        private System.Windows.Forms.NumericUpDown pr7Min;
        private System.Windows.Forms.NumericUpDown br7Max;
        private System.Windows.Forms.NumericUpDown br7Min;
        private System.Windows.Forms.Panel p8;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label l8Bpd;
        private System.Windows.Forms.Label l8Bps;
        private System.Windows.Forms.Label l8Te;
        private System.Windows.Forms.Label l8Pr;
        private System.Windows.Forms.Label l8Br;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.CheckBox m8;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.NumericUpDown bpd8Max;
        private System.Windows.Forms.NumericUpDown bpd8Min;
        private System.Windows.Forms.NumericUpDown bps8Max;
        private System.Windows.Forms.NumericUpDown bps8Min;
        private System.Windows.Forms.NumericUpDown te8Max;
        private System.Windows.Forms.NumericUpDown te8Min;
        private System.Windows.Forms.NumericUpDown pr8Max;
        private System.Windows.Forms.NumericUpDown pr8Min;
        private System.Windows.Forms.NumericUpDown br8Max;
        private System.Windows.Forms.NumericUpDown br8Min;
        private System.Windows.Forms.Button l1re;
        private System.Windows.Forms.Button l2re;
        private System.Windows.Forms.Button l3re;
        private System.Windows.Forms.Button l4re;
        private System.Windows.Forms.Button l5re;
        private System.Windows.Forms.Button l6re;
        private System.Windows.Forms.Button l7re;
        private System.Windows.Forms.Button l8re;
        private System.Windows.Forms.Button a1;
        private System.Windows.Forms.Button a3;
        private System.Windows.Forms.Button a4;
        private System.Windows.Forms.Button a5;
        private System.Windows.Forms.Button a6;
        private System.Windows.Forms.Button a7;
        private System.Windows.Forms.Button a8;
        private Label br1d;
        private Label br1s;
        private Button button7;
        private BedsideMonitorDataSet1 bedsideMonitorDataSet1;
        private BindingSource shiftBindingSource;
        private BedsideMonitorDataSet1TableAdapters.ShiftTableAdapter shiftTableAdapter;
        private Label bp1s;
        private Label bp1d;
        private Label te1s;
        private Label te1d;
        private Label pr1s;
        private Label pr1d;
        private Label bp2s;
        private Label bp2d;
        private Label te2s;
        private Label te2d;
        private Label pr2s;
        private Label pr2d;
        private Label br2s;
        private Label br2d;
        private Label bp3s;
        private Label bp3d;
        private Label te3s;
        private Label te3d;
        private Label pr3s;
        private Label pr3d;
        private Label br3s;
        private Label br3d;
        private Label bp4s;
        private Label bp4d;
        private Label te4s;
        private Label te4d;
        private Label pr4s;
        private Label pr4d;
        private Label br4s;
        private Label br4d;
        private Label bp5s;
        private Label bp5d;
        private Label te5s;
        private Label te5d;
        private Label pr5s;
        private Label pr5d;
        private Label br5s;
        private Label br5d;
        private Label bp6s;
        private Label bp6d;
        private Label te6s;
        private Label te6d;
        private Label pr6s;
        private Label pr6d;
        private Label br6s;
        private Label br6d;
        private Label bp7s;
        private Label bp7d;
        private Label te7s;
        private Label te7d;
        private Label pr7s;
        private Label pr7d;
        private Label br7s;
        private Label br7d;
        private Label bp8s;
        private Label bp8d;
        private Label te8s;
        private Label te8d;
        private Label pr8s;
        private Label pr8d;
        private Label br8s;
        private Label br8d;
        private Button a2;
    }
}


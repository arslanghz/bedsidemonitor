﻿namespace BedsideMonitor
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.bedsideMonitorDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.loginBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.shiftBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bedsideMonitorDataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bedsideMonitorDataSet1 = new BedsideMonitor.BedsideMonitorDataSet1();
            this.alarmBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.alarmTableAdapter = new BedsideMonitor.BedsideMonitorDataSet1TableAdapters.AlarmTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.bedsideMonitorDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shiftBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bedsideMonitorDataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bedsideMonitorDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(34, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Shift Log";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(115, 45);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Alarm Log";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // loginBindingSource
            // 
            this.loginBindingSource.DataMember = "Login";
            this.loginBindingSource.DataSource = this.bedsideMonitorDataSetBindingSource;
            // 
            // shiftBindingSource
            // 
            this.shiftBindingSource.DataMember = "Shift";
            this.shiftBindingSource.DataSource = this.bedsideMonitorDataSetBindingSource;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.DataSource = this.bedsideMonitorDataSet1BindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 93);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(555, 150);
            this.dataGridView1.TabIndex = 3;
            // 
            // bedsideMonitorDataSet1BindingSource
            // 
            this.bedsideMonitorDataSet1BindingSource.DataSource = this.bedsideMonitorDataSet1;
            this.bedsideMonitorDataSet1BindingSource.Position = 0;
            // 
            // bedsideMonitorDataSet1
            // 
            this.bedsideMonitorDataSet1.DataSetName = "BedsideMonitorDataSet1";
            this.bedsideMonitorDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // alarmBindingSource
            // 
            this.alarmBindingSource.DataMember = "Alarm";
            this.alarmBindingSource.DataSource = this.bedsideMonitorDataSet1BindingSource;
            // 
            // alarmTableAdapter
            // 
            this.alarmTableAdapter.ClearBeforeFill = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 412);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bedsideMonitorDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shiftBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bedsideMonitorDataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bedsideMonitorDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.BindingSource bedsideMonitorDataSetBindingSource;
//        private BedsideMonitorDataSet bedsideMonitorDataSet;
        private System.Windows.Forms.BindingSource loginBindingSource;
//        private BedsideMonitorDataSetTableAdapters.LoginTableAdapter loginTableAdapter;
        private System.Windows.Forms.BindingSource shiftBindingSource;
//        private BedsideMonitorDataSetTableAdapters.ShiftTableAdapter shiftTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource bedsideMonitorDataSet1BindingSource;
        private BedsideMonitorDataSet1 bedsideMonitorDataSet1;
        private System.Windows.Forms.BindingSource alarmBindingSource;
        private BedsideMonitorDataSet1TableAdapters.AlarmTableAdapter alarmTableAdapter;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BedsideMonitor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Login_Load(object sender, EventArgs e)
        {
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
        }
        string cs = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\GhaziArsalan(Student\Documents\BedsideMonitor.mdf;Integrated Security=True;Connect Timeout=30";
        private void button1_Click1(object sender, EventArgs e)
        {
            //Check For blank username password error
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Please provide StaffId and Password");
                return;
            }
            try
            {   
                //SQL connection database             
                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("Select * from Login where staffId=@staffId and Password=@password", con);         
                cmd.Parameters.AddWithValue("@staffId", textBox1.Text);
                cmd.Parameters.AddWithValue("@password", textBox2.Text);
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                //If count is equal to 1, than show App_Menu form
                if (count == 1)
                {
                    this.Hide();
                    frmCentralStation fm = new frmCentralStation();
                    fm.Show();
                }
                else
                {
                    MessageBox.Show("Login Failed!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Login_Load_1(object sender, EventArgs e)
        {
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ResetPassword fm = new ResetPassword();
            fm.Show();
            this.Hide();
        }
    }
}

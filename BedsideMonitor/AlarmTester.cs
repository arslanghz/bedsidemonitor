﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BedsideMonitor
{
    public class AlarmTester
    {
        public int LowerLimit
        {
            get;
            set;
        }
        public int UpperLimit
        {
            get;
            set;
        }
        public string AlarmName
        {
            get;
            private set;
        }
        String itemName;
        String initialLowerLimit;
        String initialUpperLimit;
        public AlarmTester(string itemName, float initialLowerLimit, float initialUpperLimit)
        {
            itemName = AlarmName;
            initialLowerLimit = LowerLimit;
            initialUpperLimit = UpperLimit;
        }

        string name = "value";
        public bool ValueOutsideLimits(float value)
        {
            // return true if value outside limits;
            return (false || value > UpperLimit || value < LowerLimit);
        }
    }
}
